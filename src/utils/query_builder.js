export function queryStringBuilder(q={}) {
    return Object.keys(q)
        .map(k => {
            return `${k}=${q[k]}`;
        })
        .join('&');
}