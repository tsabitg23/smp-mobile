import { Colors } from "../config/variables";

export const globalStyle = {
    card : {
        backgroundColor: Colors.white,
        borderRadius: 10,
        shadowColor: '#999999',
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    boxShadow : {
        borderRadius: 10,
        shadowColor: '#999999',
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    }
}

export const getStatusColor = (status) =>{
    if (status == 'sold') {
        return Colors.primary
    }
    else if (status == 'booking_fee_paid') {
        return Colors.warn
    }
    else if (status == 'available') {
        return '#3e2723'
    }
    return '#000';
}

export const getTaskColor = (status) => {
    if (status === 'created' || status === 'belum_selesai') {
        return Colors.warn   
    }
    else if (status === 'approved' || status === 'accepted' || status == 'selesai') {
        return Colors.primary
    }
    else if (status === 'rejected' || status === 'terkendala') {
        return Colors.danger
    }
    else {
        return '#000'
    }
}