import {BaseStore} from './base_store';

export class ProjectReportStore extends BaseStore{
    mode = 'multi';
    url = '/project_report';

    constructor(props){
        super(props);
    }

}