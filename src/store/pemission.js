import {BaseStore} from './base_store';

export class PermissionStore extends BaseStore{
    mode = 'multi';
    url = '/permission';

    constructor(props){
        super(props);
    }

}