import {action} from 'mobx';
export class Auth {
    constructor(context){
        this.context = context
        this.http = context.http;
    }
    @action
    login(email,password){
        return this.http.post('/authentication/login_mobile',{
            email,
            password
        }).then(async res=>{
            console.log(res,'this response')
            await this.context.setToken(res.token);
            await this.context.getUserPermission()
            return res
        })
    }
}