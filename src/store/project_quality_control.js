import {BaseStore} from './base_store';

export class ProjectQualityControlStore extends BaseStore{
    mode = 'multi';
    url = '/project_quality_control';

    constructor(props){
        super(props);
    }

}