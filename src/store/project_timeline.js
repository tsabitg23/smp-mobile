import {BaseStore} from './base_store';

export class ProjectTimelineStore extends BaseStore{
    mode = 'multi';
    url = '/project_timeline';

    constructor(props){
        super(props);
    }

}