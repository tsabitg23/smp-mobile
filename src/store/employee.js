import {BaseStore} from './base_store';

export class EmployeeStore extends BaseStore{
    mode = 'multi';
    url = '/employee';

    constructor(props){
        super(props);
    }

}