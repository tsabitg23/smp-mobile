import {computed,action} from 'mobx';
import {BaseStore} from './base_store';
import {appConfig} from '../config/app';
import jwt from 'react-native-pure-jwt'
import {decode as atob} from 'base-64';

export class Project extends BaseStore{
    mode = 'multi';
    url = '/project';
    
    @computed get selectedProject() {
        return this.context.userData.selected_project || "not-found";
    }

    @computed get currentProject(){
        return this.data.slice().find(it=>it.id == this.selectedProject)

    }

    constructor(props){
        super(props);
    }


    @action
    async setProject(id){
        let tokenData = JSON.parse(atob(this.context.token.split('.')[1]));
        tokenData.selected_project = id;
        return jwt.sign(tokenData, appConfig.secret, {alg: 'HS256' })
            .then((res)=>{
                this.context.setToken(res)
                return res
            })
    }
}