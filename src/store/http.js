import * as axios from 'axios';
import {appConfig} from "../config/app";


export class Http{
    constructor(context){
        this.context = context;
        this.instance = axios.create({
            baseURL: appConfig.apiUrl
        });
        this.httpConfig  = {
            headers : {
                Authorization : `Bearer ${this.context.token}`
            }
        };
    }

    async get(path){
        this.httpConfig.headers.Authorization = `Bearer ${this.context.token}`;
        let data = await this.instance.request({
            method: 'get',
            url : path,
            ...this.httpConfig
        }).catch(err=>{
            throw err.response.data;
        });

        return data.data;
    }

    async post(path,data){
        this.httpConfig.headers.Authorization = `Bearer ${this.context.token}`;
        return await this.instance.post(path,data,this.httpConfig).then(res=>{
            return res.data;
        }).catch(err=>{
            throw err.response.data;
        });
    }

    async download(path,data){
        this.httpConfig.headers.Authorization = `Bearer ${this.context.token}`;
        return await this.instance.request({
            url : path,
            method : 'post',
            data : data,
            ...this.httpConfig,
            responseType:'arraybuffer'
        }).then(res=>{
            return res;
        }).catch(err=>{
            let decodedString = String.fromCharCode.apply(null, new Uint8Array(err.response.data));
            let err_obj = JSON.parse(decodedString);
            throw err_obj;
        });
    }

    async put(path,data){
        this.httpConfig.headers.Authorization = `Bearer ${this.context.token}`;
        return await this.instance.put(path,data,this.httpConfig).then(res=>{
            return res.data;
        }).catch(err=>{
            throw err.response.data;
        });
    }

    async delete(path){
        this.httpConfig.headers.Authorization = `Bearer ${this.context.token}`;
        let data = await this.instance.request({
            method: 'delete',
            url : path,
            ...this.httpConfig
        });

        return data.data;
    }

    async upload(file){
        this.httpConfig.headers.Authorization = `Bearer ${this.context.token}`;
        let data = new FormData();
        data.append('file', file);

        return await this.instance.post('/upload',data,this.httpConfig).then(res=>{
            return res.data;
        }).catch(err=>{
            throw err.response.data;
        });
    }
}