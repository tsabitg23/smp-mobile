import {BaseStore} from './base_store';
import {computed} from 'mobx'
export class UserPermissionStore extends BaseStore{
    mode = 'multi';
    url = '/user_permission_mobile';

    constructor(props){
        super(props);
    }

}