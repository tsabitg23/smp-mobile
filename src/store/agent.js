import {BaseStore} from './base_store';

export class AgentStore extends BaseStore{
    mode = 'multi';
    url = '/agent';

    constructor(props){
        super(props);
    }

}