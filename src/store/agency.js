import {BaseStore} from './base_store';

export class AgencyStore extends BaseStore{
    mode = 'multi';
    url = '/agency';

    constructor(props){
        super(props);
    }

}