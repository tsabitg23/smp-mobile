import {BaseStore} from './base_store';

export class ProjectPaymentRequestStore extends BaseStore{
    mode = 'multi';
    url = '/project_payment_request';

    constructor(props){
        super(props);
    }

}