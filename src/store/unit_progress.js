import {BaseStore} from './base_store';

export class UnitProgressStore extends BaseStore{
    mode = 'multi';
    url = '/unit_progress';

    constructor(props){
        super(props);
    }

}