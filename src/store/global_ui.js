import {action, observable} from 'mobx';
export class GlobalUI {
    @observable isLoading = false;

    @action
    openLoader(){
        this.isLoading = true
    }

    @action
    closeLoader(){
        this.isLoading = false
    }
}