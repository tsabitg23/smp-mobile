import {observable,action, computed} from 'mobx';
import AsyncStorage from '@react-native-community/async-storage';
import {Http} from "./http";
import {Auth} from "./authentication";
import {GlobalUI} from "./global_ui";
import {decode as atob} from 'base-64';
import { Project } from './project';
import {Dashboard} from './dashboard';
import { UserStore } from './user';
import { PermissionStore } from './pemission';
import { MoMStore } from './minute_of_meeting';
import { AgencyStore } from './agency';
import { AgentStore } from './agent';
import { GuestBookStore } from './guest_book';
import { MarketingPlanStore } from './marketing_plan';
import { UnitStore } from './unit';
import { EmployeeStore } from './employee';
import { CustomerStore } from './customer';
import { UnitAkadStore } from './unit_akad';
import { ProjectReportStore } from './project_report';
import { ProjectTimelineStore } from './project_timeline';
import { UnitProgressStore } from './unit_progress';
import { ProjectPlanningStore } from './project_planning';
import { ProjectJobStore } from './project_job';
import { ProjectPaymentRequestStore } from './project_payment_request';
import { ProjectQualityControlStore } from './project_quality_control';
import { ProjectSupervisionStore } from './project_supervision';
import { EmployeeScoreStore } from './employee_score';
import { UserPermissionStore } from './user_permission';

export class MainStore{
    @observable token = '';
    
    constructor() {
        this.getToken()
    }

    http = new Http(this);
    auth = new Auth(this);
    dashboard = new Dashboard(this);
    global_ui = new GlobalUI(this);
    project = new Project(this);
    user = new UserStore(this);
    permission = new PermissionStore(this);
    notulen = new MoMStore(this);
    agency = new AgencyStore(this);
    agent = new AgentStore(this);
    guest_book = new GuestBookStore(this);
    marketing_plan = new MarketingPlanStore(this);
    unit = new UnitStore(this);
    employee = new EmployeeStore(this);
    customer = new CustomerStore(this);
    unit_akad = new UnitAkadStore(this);
    project_report = new ProjectReportStore(this);
    project_timeline = new ProjectTimelineStore(this);
    unit_progress = new UnitProgressStore(this);
    project_planning = new ProjectPlanningStore(this);
    project_job = new ProjectJobStore(this);
    project_payment_request = new ProjectPaymentRequestStore(this);
    project_quality_control = new ProjectQualityControlStore(this);
    project_supervision = new ProjectSupervisionStore(this);
    employee_score = new EmployeeScoreStore(this);
    user_permission = new UserPermissionStore(this);

    @action
    async getToken(){
        try {
            this.token = await AsyncStorage.getItem('@token')
            if(this.token){
                this.getUserPermission();
            }
            // let tokenData = JSON.parse(atob(this.token.split('.')[1]));
        } catch(e) {
            // error reading value
        }
    }

    @action
    async setToken(token){
        await AsyncStorage.setItem('@token', token);
        this.token = token;
    }

    @action
    async getUserPermission(){
        this.user_permission.getAll();
    }

    @computed 
    get userData() {
        //really important
      const token = this.token;
      if (!token) {
        return {
          user_id: '',
          role: ''
        };
      }
  
      let tokenData = JSON.parse(atob(token.split('.')[1]));
      return tokenData;
    }
}