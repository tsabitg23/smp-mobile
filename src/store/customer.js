import {BaseStore} from './base_store';

export class CustomerStore extends BaseStore{
    mode = 'multi';
    url = '/customer';

    constructor(props){
        super(props);
    }

}