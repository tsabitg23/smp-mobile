import {BaseStore} from './base_store';

export class UnitStore extends BaseStore{
    mode = 'multi';
    url = '/unit';

    constructor(props){
        super(props);
    }

}