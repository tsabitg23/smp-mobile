import {BaseStore} from './base_store';

export class UnitAkadStore extends BaseStore{
    mode = 'multi';
    url = '/unit_akad';

    constructor(props){
        super(props);
    }

}