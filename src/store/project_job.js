import {BaseStore} from './base_store';

export class ProjectJobStore extends BaseStore{
    mode = 'multi';
    url = '/project_job';

    constructor(props){
        super(props);
    }

}