import {BaseStore} from './base_store';

export class GuestBookStore extends BaseStore{
    mode = 'multi';
    url = '/guest_book';

    constructor(props){
        super(props);
    }

}