import {BaseStore} from './base_store';

export class EmployeeScoreStore extends BaseStore{
    mode = 'multi';
    url = '/employee_score';

    constructor(props){
        super(props);
    }

}