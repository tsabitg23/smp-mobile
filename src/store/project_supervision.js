import {BaseStore} from './base_store';

export class ProjectSupervisionStore extends BaseStore{
    mode = 'multi';
    url = '/project_supervision';

    constructor(props){
        super(props);
    }

}