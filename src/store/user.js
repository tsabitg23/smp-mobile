import {BaseStore} from './base_store';

export class UserStore extends BaseStore{
    mode = 'multi';
    url = '/user';

    constructor(props){
        super(props);
    }

}