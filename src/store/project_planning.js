import {BaseStore} from './base_store';

export class ProjectPlanningStore extends BaseStore{
    mode = 'multi';
    url = '/project_planning';

    constructor(props){
        super(props);
    }

}