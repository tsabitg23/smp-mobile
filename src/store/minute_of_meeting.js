import {BaseStore} from './base_store';

export class MoMStore extends BaseStore{
    mode = 'multi';
    url = '/minute_of_meeting';

    constructor(props){
        super(props);
    }

}