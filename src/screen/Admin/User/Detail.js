import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';

@inject('store')
@observer
export default class UserDetailScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'User',
        icon: "build",
    };

    async componentDidMount(){
        const {user,permission, global_ui} = this.props.store;
        const userId = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(userId){
            console.log(userId);
            await user.getDetail(userId);
            await permission.getAll();
        }
        console.log(user.selectedData);
        global_ui.closeLoader();
    }

    renderPermission = (data) => {
        const {permission} = this.props.store
        const permissionData = permission.data.find(it=>it.id == data.permission_id);
        return (
            <CardItem key={data.id}>
                <Body>
                    <Text style={[human.body]}>{startCase(get(permissionData,'name','-'))}</Text>
                    <Text style={[human.footnote]}><Icon name={data.read ? 'checkmark' : 'close'} style={{color : data.read ? Colors.primary : 'red', fontSize: 12}}/> Read</Text>
                    <Text style={[human.footnote]}><Icon name={data.create ? 'checkmark' : 'close'} style={{color : data.create ? Colors.primary : 'red', fontSize: 12}}/> Create</Text>
                    <Text style={[human.footnote]}><Icon name={data.update ? 'checkmark' : 'close'} style={{color : data.update ? Colors.primary : 'red', fontSize: 12}}/> Update</Text>
                    <Text style={[human.footnote]}><Icon name={data.delete ? 'checkmark' : 'close'} style={{color : data.delete ? Colors.primary : 'red', fontSize: 12}}/> Delete</Text>
                </Body>
            </CardItem>
        )
    }


    render() {
        const {user} = this.props.store;
        const infoCardData = [{
            label : 'Email',
            value : get(user.selectedData,'email','-')
        },{
            label : 'Role',
            value : get(user.selectedData,'role.name','-')
        }]
        const userPermission = get(user.selectedData,'permissions',[]);
        return (
            <Container>
                <Content>
                    <ModalHeader title={"User Detail"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>User Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Permission</Text>
                        </View>
                        <Card>
                            {userPermission.map(it=>{
                                return this.renderPermission(it)
                            })}
                        </Card>
                    </View>
                </Content>
            </Container>
        );
    }
}
