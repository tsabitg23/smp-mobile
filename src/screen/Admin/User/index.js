import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';

@inject('store')
@observer
export default class UserScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'User',
        icon: "build",
    };

    async componentDidMount(){
        const {user, global_ui} = this.props.store;
        global_ui.openLoader();
        await user.getAll();
        global_ui.closeLoader();
    }

    renderItem = ({item, index})=>{
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('UserDetail', {
                    id : item.id
                })}>
                    <Body>
                        <Text style={[human.body]}>
                            {item.email}
                        </Text>
                        <View style={{flexDirection:'row',alignItems:'center',marginTop: 5}}>
                            <Text style={[human.footnote, systemWeights.light]}>
                                {item.role.name}
                            </Text>
                        </View>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    componentWillUnmount(){
        this.props.store.user.changeSearchData(false);
    }

    state = {
        isFilterVisible : false
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.user.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.user.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"User"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Tanggal",
                        value : "created_at"
                    },{
                        label : "Email",
                        value : "email"
                    }]}
                    searchByColumns={[{
                        label : "Email",
                        value : "email"
                    }]}
                    defaultOrderBy={this.props.store.user.orderBy}
                    defaultOrder={this.props.store.user.order}
                    defaultSearchBy={'email'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1,padding : 10}}>
                    <FlatList
                        data={this.props.store.user.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
