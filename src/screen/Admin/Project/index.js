import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList, Image} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';

@inject('store')
@observer
export default class Project extends Component {
    static navigationOptions = {
        drawerLabel: 'Project',
        icon: "build",
    };

    componentWillUnmount(){
        this.props.store.project.changeSearchData(false);
    }

    async componentDidMount(){
        const {project, global_ui} = this.props.store;
        global_ui.openLoader();
        await project.getAll();
        global_ui.closeLoader();
    }

    state = {
        isFilterVisible : false
    }

    renderItem = ({item})=>{
        return (
            <Card style={{borderRadius: 5}}>
                <CardItem>
                    <Body>
                        <Text style={[human.body]}>
                            {item.name}
                        </Text>
                        <View style={{flexDirection:'row',alignItems:'center',marginTop: 5}}>
                            <Icon name="pin" style={{fontSize:12, color: Colors.primary}}/>
                            <Text style={[human.footnote, systemWeights.light]}>
                            {" "}{item.location}
                            </Text>
                        </View>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.project.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.project.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"Project"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Tanggal",
                        value : "created_at"
                    },{
                        label : "Nama",
                        value : "name"
                    }]}
                    searchByColumns={[{
                        label : "Nama",
                        value : "name"
                    },{
                        label : "Lokasi",
                        value : "location"
                    }]}
                    defaultOrderBy={this.props.store.project.orderBy}
                    defaultOrder={this.props.store.project.order}
                    defaultSearchBy={'name'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1, padding : 10}}>
                    <FlatList
                        data={this.props.store.project.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
