/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import DrawerHeader from "../../Component/Header/DrawerHeader";
import {Colors} from "../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import PageNavigation from '../../Component/PageNavigation';

@inject('store')
@observer
export default class AdminScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Admin',
        icon: "person",
        permissionKey : 'admin'
    };

    async componentDidMount(){
    }

    render() {
        const {user_permission} = this.props.store;
        const menu = [{
            name : 'Project',
            iconColor : '#0288d1',
            icon : 'build',
            backgroundColor : 'rgba(2, 136, 209, 0.2)',
            route: 'Project',
            permissionKey: 'projects'
        },{
            name : 'User',
            iconColor : '#00c853',
            icon : 'person',
            backgroundColor : 'rgba(0, 200, 83, 0.2)',
            route : 'User',
            permissionKey: 'users'
        },]
        
        const menuPermissionKey = 'admin';
        const submenuPermission = user_permission.data.find(it=>it.key == menuPermissionKey);
        const filteredMenu = menu.filter(it=>{
            const permissionIndex = submenuPermission.submenu.findIndex(prop=>prop.key == it.permissionKey);
            return permissionIndex > -1
        });

        return (
            <View>
                <DrawerHeader title={"Admin"} navigation={this.props.navigation}/>
                <View style={{padding : 20}}>
                    <PageNavigation menu={filteredMenu} navigation={this.props.navigation}/>
                </View>
            </View>
        );
    }
}
