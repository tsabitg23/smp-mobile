/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Dimensions} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon,Card, CardItem } from 'native-base';
import DrawerHeader from "../../Component/Header/DrawerHeader";
import {Colors} from "../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import { globalStyle } from '../../utils/styles';
import {get as _get} from 'lodash';
import Carousel from 'react-native-snap-carousel';
import {kebabCase, startCase} from 'lodash'
import {
    PieChart,
  } from 'react-native-chart-kit'

const {height, width} = Dimensions.get('window');
@inject('store')
@observer
export default class Home extends Component {
    static navigationOptions = {
        drawerLabel: 'Beranda',
        icon: "home",
        permissionKey : 'for_all'
    };

    async componentDidMount(){
        const {project, dashboard, global_ui} = this.props.store;
        try {
            global_ui.openLoader();
            await project.getAll();
            await dashboard.getAll();
            global_ui.closeLoader();
        } 
        catch (err) {
            global_ui.closeLoader();
        }
    }

    async componentDidUpdate(prev){
        const {project, dashboard, global_ui} = this.props.store;
        if(this.state.selectedProject !== project.selectedProject){
            this.setState({
                selectedProject : project.selectedProject
            })
            global_ui.openLoader();
            await dashboard.getAll();
            global_ui.closeLoader();
        }
    }

    state = {
        slider1Item : 1,
        selectedProject: this.props.store.project.selectedProject
    }

    getIcon = (icon) => {
        return kebabCase(icon);
    }

    getColor = (index) => {
        const colorList = ['#83a7ea','#f44336', '#9c27b0', '#2196f3','#009688','#ffeb3b', '#4caf50', '#795548'];
        return colorList[index] || '#757575'
    }

    dashboardContent = {
        count : (item)=>{
            return (
                <CardItem style={{height:'100%'}}>
                        <Body style={{display:'flex',justifyContent: 'space-between'}}>
                            <View style={{flex: 1, justifyContent:'center'}}>
                                <Text style={[human.body, systemWeights.semibold, {fontSize:22}]}> {item.value}</Text>
                            </View>
                            <View style={{flexDirection: 'row',}}>
                                <View style={{marginRight: 3}}>
                                    <Icon type={"MaterialIcons"} name={this.getIcon(item.icon)} style={{fontSize:15, color: Colors.primary}}/>
                                </View>
                                <View>
                                    <Text style={[human.subhead]}>
                                        {item.title}
                                    </Text>
                                    <View style={{flexDirection:'row',alignItems:'center',marginTop: 5}}>
                                        {/* <Icon name="pin" style={{fontSize:12, color: Colors.primary}}/> */}
                                        <Text style={[human.caption1, systemWeights.light]}>
                                            {item.subtitle}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </Body>
                    </CardItem>
            )
        },
        pie : (item)=>{
            return (
                <CardItem style={{height:'100%'}}>
                    <Body style={{display:'flex',justifyContent: 'space-between'}}>
                        <View style={{flex: 1, justifyContent:'center'}}>
                            <Text style={[human.body, systemWeights.semibold, {fontSize:22}]}> YEY</Text>
                        </View>
                        <View style={{flexDirection: 'row',}}>
                            <View style={{marginRight: 3}}>
                                <Icon type={"MaterialIcons"} name={this.getIcon(item.icon)} style={{fontSize:15, color: Colors.primary}}/>
                            </View>
                            <View>
                                <Text style={[human.subhead]}>
                                    {item.title}
                                </Text>
                                <View style={{flexDirection:'row',alignItems:'center',marginTop: 5}}>
                                    {/* <Icon name="pin" style={{fontSize:12, color: Colors.primary}}/> */}
                                    <Text style={[human.caption1, systemWeights.light]}>
                                        {item.subtitle}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </Body>
                </CardItem>
            )
        }
    }

    _renderItem = ({item, index}) => {
        return (
            <Card style={{height :125,marginRight: 10}}>
                {
                    this.dashboardContent[item.type](item)        
                }
            </Card>
        )
    }

    render() {
        const {project, dashboard} = this.props.store;
        return (
            <View>
                <DrawerHeader title={"Beranda"} navigation={this.props.navigation}/>
                <View style={{padding : 20}}>
                    <View style={{marginBottom: 20}}>
                        <Text style={[human.subhead, systemWeights.regular,{color: Colors.passive, paddingBottom : 10}]}>Project</Text>
                        <View style={[globalStyle.card]}>
                            <CardItem>
                                <Body>
                                    <Text style={[human.headline, systemWeights.semibold]}>
                                        {(project.currentProject && project.currentProject.name) ? project.currentProject.name : '-'}
                                    </Text>
                                    <View style={{flexDirection:'row',alignItems:'center',marginTop: 5}}>
                                        <Icon name="pin" style={{fontSize:12, color: Colors.primary}}/>
                                        <Text style={[human.caption1, systemWeights.light]}>
                                        {" "+_get(project.currentProject,'location','project location')}
                                        </Text>
                                    </View>
                                </Body>
                            </CardItem>
                        </View>
                    </View>
                    
                    <View style={{marginBottom: 20}}>
                        <Text style={[human.subhead, systemWeights.regular,{color: Colors.passive, paddingBottom : 10}]}>Informasi</Text>

                        <Carousel
                            ref={(c) => { this._carousel = c; }}
                            data={dashboard.data.filter(it=>it.type === 'count')}
                            renderItem={this._renderItem}
                            sliderWidth={width}
                            inactiveSlideOpacity={0.9}
                            itemWidth={width / 2}
                            loop={true}
                            loopClonesPerSide={3}
                            activeSlideAlignment="center"
                            enableMomentum={true}
                            onSnapToItem={(index) => this.setState({ slider1Item: index }) }
                        />
                    </View>

                    {
                        dashboard.data.filter(it=>it.type === 'pie' && it.value.length > 0).map((it)=>{
                            const chartData = it.value.map((data,index)=>{
                                return {
                                    name : startCase(data.name),
                                    value : (+data.value),
                                    color : this.getColor(index),
                                    legendFontColor : '#7F7F7F',
                                    legendFontSize : 13
                                }
                            })

                            return (
                                <View style={{marginBottom: 20}} key={it.title}>
                                    <Text style={[human.subhead, systemWeights.regular,{color: Colors.passive, paddingBottom : 10}]}>{it.title}</Text>
                                    <PieChart
                                        data={chartData}
                                        width={width*90/100}
                                        height={220}
                                        chartConfig={{
                                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                            backgroundColor: '#e26a00',
                                            backgroundGradientFrom: '#fb8c00',
                                            backgroundGradientTo: '#ffa726',
                                        }}
                                        accessor="value"
                                        backgroundColor="transparent"
                                        paddingLeft="15"
                                    />
                                </View>
                            )
                        })
                    }

                </View>

            </View>
        );
    }
}
