import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import { getTaskColor } from '../../../utils/styles';

@inject('store')
@observer
export default class ProjectTimelineDetailScreen extends Component {

    async componentDidMount(){
        const {project_timeline, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await project_timeline.getDetail(id);
        }
        console.log(project_timeline.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {project_timeline} = this.props.store;
        const date = get(project_timeline.selectedData,'date',false);
        const status = get(project_timeline.selectedData,'status','-');
        const infoCardData = [{
            label : 'Kegiatan',
            value : get(project_timeline.selectedData,'name','-')
        },{
            label : 'Penanggung Jawab',
            value : get(project_timeline.selectedData,'employee.name','-')
        },{
            label : 'Tanggal',
            value : date ? moment(date).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Status',
            value :  startCase(status),
            valueColor : status == 'open' ? Colors.warn : Colors.primary
        }]
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Detail Timeline"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
