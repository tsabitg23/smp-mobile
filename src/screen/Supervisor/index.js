/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList, ScrollView} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import DrawerHeader from "../../Component/Header/DrawerHeader";
import {Colors} from "../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import PageNavigation from '../../Component/PageNavigation';

@inject('store')
@observer
export default class SupervisorScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Supervisor',
        icon: "checkbox",
        permissionKey: 'supervisor'
    };

    async componentDidMount(){
    
    }

    render() {
        const {user_permission} = this.props.store;
        const menu = [{
            name : 'Project Timeline',
            iconColor : '#0288d1',
            icon : 'calendar',
            backgroundColor : 'rgba(2, 136, 209, 0.2)',
            route: 'ProjectTimeline',
            permissionKey : 'timeline_project'
        },{
            name : 'Unit Progress',
            iconColor : '#00c853',
            icon : 'checkmark-circle',
            backgroundColor : 'rgba(0, 200, 83, 0.2)',
            route : 'UnitProgress',
            permissionKey : 'unit_progress'
        },{
            name : 'Pengawasan Project',
            iconColor : '#9c27b0',
            icon : 'construct',
            backgroundColor : 'rgba(156, 39, 176, 0.2)',
            route : 'ProjectSupervisor',
            permissionKey : 'project_plannings'
        },{
            name : 'Kebutuhan Project',
            iconColor : '#b71c1c',
            icon : 'add',
            backgroundColor : 'rgba(183, 28, 28, 0.2)',
            route : 'ProjectJob',
            permissionKey : 'project_jobs'
        },{
            name : 'Pembiayaan Project',
            iconColor : '#ffa000',
            icon : 'cash',
            backgroundColor : 'rgba(253, 216, 53, 0.2)',
            route : 'ProjectPaymentRequest',
            permissionKey : 'project_payment_request'
        },{
            name : 'Quality Control',
            iconColor : '#4e342e',
            icon : 'checkbox',
            backgroundColor : 'rgba(78, 52, 46, 0.2)',
            route : 'QualityControl',
            permissionKey : 'project_quality_control'
        },{
            name : 'Laporan',
            iconColor : '#e91e63',
            icon : 'warning',
            backgroundColor : 'rgba(233, 30, 99, 0.2)',
            route : 'Report',
            permissionKey : 'project_report'
        },]
        
        const menuPermissionKey = 'supervisor';
        const submenuPermission = user_permission.data.find(it=>it.key == menuPermissionKey);
        const filteredMenu = menu.filter(it=>{
            const permissionIndex = submenuPermission.submenu.findIndex(prop=>prop.key == it.permissionKey);
            return permissionIndex > -1
        });
        return (
            <ScrollView>
                <DrawerHeader title={"Supervisor"} navigation={this.props.navigation}/>
                <View style={{padding : 20}}>
                    <PageNavigation menu={filteredMenu} navigation={this.props.navigation}/>
                </View>
            </ScrollView>
        );
    }
}
