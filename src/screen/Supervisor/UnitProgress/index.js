import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import moment from 'moment';
import {startCase} from 'lodash';
import { getTaskColor } from '../../../utils/styles';

@inject('store')
@observer
export default class UnitProgressScreen extends Component {

    async componentDidMount(){
        const {unit_progress, global_ui} = this.props.store;
        global_ui.openLoader();
        await unit_progress.getAll();
        global_ui.closeLoader();
    }

    renderItem = ({item, index})=>{
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('UnitProgressDetail', {
                    id : item.id
                })}>
                    <Body>
                        <Text style={[human.body]}>
                            {item.unit.kavling_number}
                        </Text>
                        <Text style={[human.footnote, systemWeights.light]}>
                            Progress : {item.percentage}%
                        </Text>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    componentWillUnmount(){
        this.props.store.unit_progress.changeSearchData(false);
        // this.props.store.guest_book.changeFilterKey('orderBy', 'date');
    }

    state = {
        isFilterVisible : false
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.unit_progress.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.unit_progress.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"Unit Progress"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Tanggal",
                        value : "create_at"
                    },{
                        label : "Unit",
                        value : "unit.kavling_number"
                    },{
                        label : "Persentase",
                        value : "percentage"
                    }]}
                    searchByColumns={[{
                        label : "Tanggal",
                        value : "created_at"
                    },{
                        label : "Unit",
                        value : "unit.kavling_number"
                    }]}
                    defaultOrderBy={this.props.store.unit_progress.orderBy}
                    defaultOrder={this.props.store.unit_progress.order}
                    defaultSearchBy={'unit.kavling_number'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1,padding : 10}}>
                    <FlatList
                        data={this.props.store.unit_progress.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
