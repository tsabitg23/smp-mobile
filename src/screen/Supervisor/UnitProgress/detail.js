import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import { getTaskColor } from '../../../utils/styles';

@inject('store')
@observer
export default class UnitProgressDetailScreen extends Component {

    async componentDidMount(){
        const {unit_progress, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await unit_progress.getDetail(id);
        }
        console.log(unit_progress.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {unit_progress} = this.props.store;
        const infoCardData = [{
            label : 'Nomor Kavling Unit',
            value : get(unit_progress.selectedData,'unit.kavling_number','-')
        },{
            label : 'Persentase',
            value : get(unit_progress.selectedData,'percentage','-') + '%'
        },{
            label : 'description',
            value : get(unit_progress.selectedData,'description','-')
        }]

        return (
            <Container>
                <Content>
                    <ModalHeader title={"Detail Progress"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
