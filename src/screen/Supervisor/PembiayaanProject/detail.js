import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import { getTaskColor } from '../../../utils/styles';

@inject('store')
@observer
export default class ProjectPaymentRequestDetailScreen extends Component {

    async componentDidMount(){
        const {project_payment_request, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await project_payment_request.getDetail(id);
        }
        console.log(project_payment_request.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {project_payment_request} = this.props.store;
        const status = get(project_payment_request.selectedData,'task.status','-');
        const deadline = get(project_payment_request.selectedData,'deadline',false);
        const created_at = get(project_payment_request.selectedData,'deadline',false);
        const infoCardData = [{
            label : 'Kebutuhan',
            value : get(project_payment_request.selectedData,'name','-')
        },{
            label : 'Tanggal Request',
            value : created_at ? moment(created_at).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Deadline Pembayaran',
            value : deadline ? moment(deadline).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Total Biaya',
            value : 'Rp.' + get(project_payment_request.selectedData,'max_cost','-')
        },{
            label : 'Deskripsi',
            value : get(project_payment_request.selectedData,'description','-')
        },,{
            label : 'Status',
            value : status == 'created' ? 'Pending' : startCase(status),
            valueColor : getTaskColor(status)
        }];

        return (
            <Container>
                <Content>
                    <ModalHeader title={"Detail Pembiayaan"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
