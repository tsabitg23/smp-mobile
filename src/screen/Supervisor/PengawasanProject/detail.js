import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import {getTaskColor} from '../../../utils/styles';

@inject('store')
@observer
export default class ProjectPlanningDetailScreen extends Component {

    async componentDidMount(){
        const {project_planning, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await project_planning.getDetail(id);
        }
        console.log(project_planning.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {project_planning} = this.props.store;
        const start_date = get(project_planning.selectedData,'start_date',false);
        const approx_finish_date = get(project_planning.selectedData,'approx_finish_date',false);
        const status = get(project_planning.selectedData,'status','-');
        
        const infoCardData = [{
            label : 'Pekerjaan',
            value : get(project_planning.selectedData,'name','-')
        },{
            label : 'Tanggal Mulai',
            value : start_date ? moment(start_date).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Rencana Selesai',
            value : approx_finish_date ? moment(approx_finish_date).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Progress',
            value : get(project_planning.selectedData,'percentage','-') + '%'
        },{
            label : 'Deskripsi',
            value : get(project_planning.selectedData,'description','-')
        },{
            label : 'Status',
            value : status,
            valueColor : getTaskColor(status)
        }]
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Detail Pengawasan"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
