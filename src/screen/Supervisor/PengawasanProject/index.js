import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import moment from 'moment';
import {startCase} from 'lodash';
import { getTaskColor } from '../../../utils/styles';

@inject('store')
@observer
export default class ProjectPlanningScreen extends Component {

    async componentDidMount(){
        const {project_planning, global_ui} = this.props.store;
        global_ui.openLoader();
        await project_planning.getAll();
        global_ui.closeLoader();
    }

    renderItem = ({item, index})=>{
        const status = item.status;
        const taskColor = getTaskColor(status); 
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('ProjectSupervisorDetail', {
                    id : item.id
                })}>
                    <Body>
                        <Text style={[human.body]}>
                            {item.name}
                        </Text>
                        <Text style={[human.footnote, systemWeights.light]}>
                            Tanggal Mulai : {moment(item.start_date).format('dddd, DD-MM-YYYY')}
                        </Text>
                        <Text style={[human.footnote, systemWeights.light]}>
                            Rencana Selesai : {moment(item.approx_finish__date).format('dddd, DD-MM-YYYY')}
                        </Text>
                        <Text style={[human.footnote, systemWeights.light]}>
                            Progress : {item.percentage}%
                        </Text>
                        <Text style={[human.footnote, systemWeights.semibold, {color : taskColor}]}>
                            {startCase(status)}
                        </Text>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    componentWillUnmount(){
        this.props.store.project_planning.changeSearchData(false);
        this.props.store.project_planning.changeFilterKey('orderBy', 'start_date');
    }

    state = {
        isFilterVisible : false
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.project_planning.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.project_planning.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"Pengawasan Project"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Tanggal Mulai",
                        value : "start_date"
                    },{
                        label : "Tanggal Selesai",
                        value : "approx_finish_date"
                    },{
                        label : "Status",
                        value : "status"
                    },{
                        label : "Progress",
                        value : "percentage"
                    }]}
                    searchByColumns={[{
                        label : "Tanggal Mulai",
                        value : "start_date"
                    },{
                        label : "Tanggal Selesai",
                        value : "approx_finish_date"
                    },{
                        label : "Pekerjaan",
                        value : "name"
                    }]}
                    defaultOrderBy={'start_date'}
                    defaultOrder={this.props.store.project_planning.order}
                    defaultSearchBy={'start_date'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1,padding : 10}}>
                    <FlatList
                        data={this.props.store.project_planning.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
