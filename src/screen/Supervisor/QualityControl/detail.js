import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import { getTaskColor } from '../../../utils/styles';

@inject('store')
@observer
export default class ProjectQualityControlDetailScreen extends Component {

    async componentDidMount(){
        const {project_quality_control, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await project_quality_control.getDetail(id);
        }
        console.log(project_quality_control.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {project_quality_control} = this.props.store;

        const infoCardData = [{
            label : 'Temuan',
            value : get(project_quality_control.selectedData,'name','-')
        },{
            label : 'Kategori',
            value : get(project_quality_control.selectedData,'type','-')
        },{
            label : 'Dampak Temuan',
            value : get(project_quality_control.selectedData,'impact','-')
        },{
            label : 'Description',
            value : get(project_quality_control.selectedData,'description','-')
        }];

        return (
            <Container>
                <Content>
                    <ModalHeader title={"Detail Quality Control"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
