import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import { getTaskColor } from '../../../utils/styles';

@inject('store')
@observer
export default class ReportDetailScreen extends Component {

    async componentDidMount(){
        const {project_report, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await project_report.getDetail(id);
        }
        console.log(project_report.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {project_report} = this.props.store;
        const created_at = get(project_report.selectedData,'created_at',false);
        const status = get(project_report.selectedData,'task.status','-');
        const infoCardData = [{
            label : 'Temuan',
            value : get(project_report.selectedData,'name','-')
        },{
            label : 'Kategori',
            value : get(project_report.selectedData,'type','-')
        },{
            label : 'Rekomendasi',
            value : get(project_report.selectedData,'recommendation','-')
        },{
            label : 'Rekomendasi Manager',
            value : get(project_report.selectedData,'alternate_recommendation','-') || '-'
        },{
            label : 'Status',
            value : status == 'created' ? 'Pending' : startCase(status),
            valueColor : getTaskColor(status)
        }]
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Detail Laporan"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
