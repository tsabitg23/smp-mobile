import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import moment from 'moment';
import {startCase} from 'lodash';
import { getTaskColor } from '../../../utils/styles';

@inject('store')
@observer
export default class ProjectJobScreen extends Component {

    async componentDidMount(){
        const {project_job, global_ui} = this.props.store;
        global_ui.openLoader();
        await project_job.getAll();
        global_ui.closeLoader();
    }

    renderItem = ({item, index})=>{
        const status = item.task.status;
        const taskColor = getTaskColor(status);
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('ProjectJobDetail', {
                    id : item.id
                })}>
                    <Body>
                        <Text style={[human.body]}>
                            {item.name}
                        </Text>
                        <Text style={[human.footnote, systemWeights.light]}>
                            {moment(item.request_date).format('dddd, DD-MM-YYYY')}
                        </Text>
                        <Text style={[human.footnote, systemWeights.semibold, {color : taskColor}]}>
                            {status == 'created' ? 'Pending' : startCase(status)}
                        </Text>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    componentWillUnmount(){
        this.props.store.project_job.changeSearchData(false);
    }

    state = {
        isFilterVisible : false
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.project_job.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.project_job.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"Kebutuhan Project"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Tanggal",
                        value : "created_at"
                    },{
                        label : "Kebutuhan",
                        value : "name"
                    }]}
                    searchByColumns={[{
                        label : "Kebutuhan",
                        value : "name"
                    }]}
                    defaultOrderBy={this.props.store.project_job.orderBy}
                    defaultOrder={this.props.store.project_job.order}
                    defaultSearchBy={'name'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1,padding : 10}}>
                    <FlatList
                        data={this.props.store.project_job.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
