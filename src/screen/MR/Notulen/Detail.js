import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
@inject('store')
@observer
export default class NotulenDetailScreen extends Component {

    async componentDidMount(){
        const {notulen, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await notulen.getDetail(id);
        }
        global_ui.closeLoader();
    }

    render() {
        const {notulen} = this.props.store;
        const infoCardData = [{
            label : 'Tanggal',
            value : moment(get(notulen.selectedData,'meeting_date','1990-01-01')).format('dddd, DD MM YYYY')
        },{
            label : 'Notulen',
            value : get(notulen.selectedData,'description','-')
        }]
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Notulen Detail"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Notulen</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
