import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import moment from 'moment';

@inject('store')
@observer
export default class NotulenScreen extends Component {

    async componentDidMount(){
        const {notulen, global_ui} = this.props.store;
        global_ui.openLoader();
        await notulen.getAll();
        global_ui.closeLoader();
    }

    renderItem = ({item, index})=>{
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('NotulenDetail', {
                    id : item.id
                })}>
                    <Body>
                        <Text style={[human.body]}>
                            {moment(item.meeting_date).format('dddd, DD MM YYYY')}
                        </Text>
                        <View style={{flexDirection:'row',alignItems:'center',marginTop: 5}}>
                            <Text style={[human.footnote, systemWeights.light]}>
                                {item.description.length > 50 ? (item.description.substring(0,50) + "...") : item.description}
                            </Text>
                        </View>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    componentWillUnmount(){
        this.props.store.notulen.changeSearchData(false);
    }

    state = {
        isFilterVisible : false
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.notulen.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.notulen.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"Notulen"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Tanggal",
                        value : "meeting_date"
                    }]}
                    searchByColumns={[{
                        label : "Tanggal",
                        value : "meeting_date"
                    }]}
                    defaultOrderBy={'meeting_date'}
                    defaultOrder={this.props.store.notulen.order}
                    defaultSearchBy={'meeting_date'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1,padding : 10}}>
                    <FlatList
                        data={this.props.store.notulen.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
