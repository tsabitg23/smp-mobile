import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import { getTaskColor } from '../../../utils/styles';

@inject('store')
@observer
export default class ManagementSupervisionDetailScreen extends Component {

    async componentDidMount(){
        const {project_supervision, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await project_supervision.getDetail(id);
        }
        console.log(project_supervision.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {project_supervision} = this.props.store;

        const infoCardData = [{
            label : 'Aktivitas Yang Dipantau',
            value : get(project_supervision.selectedData,'name','-')
        },{
            label : 'Divisi',
            value : get(project_supervision.selectedData,'division','-')
        },{
            label : 'Kondisi',
            value : get(project_supervision.selectedData,'result','-')
        },{
            label : 'Deskripsi',
            value : get(project_supervision.selectedData,'description','-')
        }];

        return (
            <Container>
                <Content>
                    <ModalHeader title={"Detail Pengawasan"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
