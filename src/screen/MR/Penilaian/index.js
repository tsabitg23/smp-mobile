import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import moment from 'moment';
import {startCase} from 'lodash';
import { getTaskColor } from '../../../utils/styles';

@inject('store')
@observer
export default class EmployeeScoreScreen extends Component {

    async componentDidMount(){
        const {employee_score, global_ui} = this.props.store;
        global_ui.openLoader();
        await employee_score.getAll();
        global_ui.closeLoader();
    }

    renderItem = ({item, index})=>{
        const status = item.status;
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('EmployeeScoreDetail', {
                    id : item.id
                })}>
                    <Body>
                        <Text style={[human.body]}>
                            {item.employee.name}
                        </Text>
                        <Text style={[human.footnote, systemWeights.light]}>
                            Periode : {moment(item.periode_start).format('dddd, DD-MM-YYYY')} - {moment(item.periode_end).format('dddd, DD-MM-YYYY')} 
                        </Text>
                        <Text style={[human.footnote, systemWeights.light]}>
                            Total Skor : {item.total_score} 
                        </Text>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    componentWillUnmount(){
        this.props.store.employee_score.changeSearchData(false);
        this.props.store.guest_book.changeFilterKey('orderBy', 'periode_start');
    }

    state = {
        isFilterVisible : false
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.employee_score.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.employee_score.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"Penilaian"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Periode Mulai",
                        value : "periode_start"
                    },{
                        label : "Periode Selesai",
                        value : "periode_end"
                    },{
                        label : "Nama Pegawai",
                        value : "employee.name"
                    },{
                        label : "Total Skor",
                        value : "total_score"
                    }]}
                    searchByColumns={[{
                        label : "Nama Pegawai",
                        value : "employee.name"
                    }]}
                    defaultOrderBy={'periode_start'}
                    defaultOrder={this.props.store.employee_score.order}
                    defaultSearchBy={'employee.name'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1,padding : 10}}>
                    <FlatList
                        data={this.props.store.employee_score.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
