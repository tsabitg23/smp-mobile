import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem, Button } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import Collapsible from 'react-native-collapsible';
import CollapsibleCard from '../../../Component/Card/CollapsibleCard';
import CollapsibleScoreCard from '../../../Component/Card/CollapsibleEmployeeScore';

@inject('store')
@observer
export default class EmployeeScoreDetailScreen extends Component {

    async componentDidMount(){
        const {employee_score, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await employee_score.getDetail(id);
        }
        console.log(employee_score.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {employee_score} = this.props.store;
        const periode_start = get(employee_score.selectedData,'periode_start',false);
        const periode_end = get(employee_score.selectedData,'periode_end',false);

        const periodeStartDate = periode_start ? moment(periode_start).format('dddd, DD-MM-YYYY') : '-';
        const periodeEndDate = periode_end ? moment(periode_end).format('dddd, DD-MM-YYYY') : '-';
        // const id_card_type = get(employee_score.selectedData,'id_card_type','-');
        const infoCardData = [{
            label : 'Nama Pegawai',
            value : get(employee_score.selectedData,'employee.name','-')
        },{
            label : 'Periode Penilaian',
            value : periodeStartDate + " - " + periodeEndDate
        },{
            label : 'Total Skor',
            value : get(employee_score.selectedData,'total_score','-')
        },{
            label : 'Catatan Atasan',
            value : get(employee_score.selectedData,'boss_note','-')
        },{
            label : 'Catatan HRD',
            value : get(employee_score.selectedData,'hrd_note','-')
        },{
            label : 'Catatan Pegawai',
            value : get(employee_score.selectedData,'employee_note','-')
        }];

        const questionList = get(employee_score.selectedData, 'question_list',[]);

        const penilaianCard = questionList.map(it=>{
            return {
                label : it.number + " " + it.name,
                value : it.score,
                isHeader : it.is_header
            }
        })

        // const phones_data = JSON.parse(get(employee_score.selectedData,'phones_data',"{}"));
        // const contactCardData = [{
        //     label : "Telepon Rumah",
        //     value : get(phones_data,'home','-')
        // },{
        //     label : "Handphone",
        //     value : get(phones_data,'handphone','-')
        // },{
        //     label : "Whatsapp",
        //     value : get(phones_data,'whatsapp','-')
        // },];

        // const company_data = JSON.parse(get(employee_score.selectedData,'company_data',"{}"));
        // const companyCardData = [{
        //     label : "Pekerjaan",
        //     value : get(company_data,'occupation','-')
        // },{
        //     label : "Nama Perusahaan",
        //     value : get(company_data,'company_name','-')
        // },{
        //     label : "Bidang Perusahaan",
        //     value : get(company_data,'company_field','-')
        // },{
        //     label : "Alamat Kantor",
        //     value : get(company_data,'company_address','-')
        // },{
        //     label : "Telepon Kantor",
        //     value : get(company_data,'company_phone','-')
        // },{
        //     label : "Lama Bekerja",
        //     value : get(company_data,'work_duration','-')
        // },];

        // const contactable_data = JSON.parse(get(employee_score.selectedData,'contactable_data',"{}"));
        // const contactableCardData = [{
        //     label : "Nama Kerabat",
        //     value : get(contactable_data,'name','-')
        // },{
        //     label : "Hubungan",
        //     value : get(contactable_data,'relationship','-')
        // },{
        //     label : "Telepon Rumah",
        //     value : get(contactable_data,'phone_home','-')
        // },{
        //     label : "Handphone",
        //     value : get(contactable_data,'handphone','-')
        // },{
        //     label : "Whatsapp",
        //     value : get(contactable_data,'whatsapp','-')
        // },{
        //     label : "Alamat Surat Menyurat",
        //     value : get(contactable_data,'mail_address','-')
        // },{
        //     label : "Status Alamat",
        //     value : get(contactable_data,'address_status','-')
        // },{
        //     label : "Alamat",
        //     value : get(contactable_data,'address','-')
        // },];

        // const financial_data = JSON.parse(get(employee_score.selectedData,'financial_data',"{}"));
        // const financialCardData = [{
        //     label : "Pendapatan Per Bulan",
        //     value : get(financial_data,'monthly_income','-')
        // },{
        //     label : "Pendapatan Pasangan Per Bulan",
        //     value : get(financial_data,'spouse_monthly_income','-')
        // },{
        //     label : "Pengeluaran Per Bulan",
        //     value : get(financial_data,'monthly_expense','-')
        // },{
        //     label : "Angsuran Lain. terbayar",
        //     value : get(financial_data,"other_installment_pay",'-')
        // },{
        //     label : "Angsuran Lain. Dari",
        //     value : get(financial_data,"other_installment_total",'-')
        // },{
        //     label : "Tanggungan",
        //     value : get(financial_data,"dependents",'-')
        // },];

        // const employee_scoreUnits = get(employee_score.selectedData,'units',[]);

        return (
            <Container>
                <Content>
                    <ModalHeader title={"Detail Penilaian"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                        <CollapsibleScoreCard data={penilaianCard} subtitle={"Kriteria Penilaian : 1 = Buruk, 2 = Sedang, 3 = Baik, 4 = Sangat Baik"} title={"Penilaian Kinerja Karyawan"}/>
                        {/* <CollapsibleCard data={companyCardData} title={"Pekerjaan"}/>
                        <CollapsibleCard data={contactableCardData} title={"Kontak Kerabat"}/>
                        <CollapsibleCard data={financialCardData} title={"Finansial"}/> */}
                    </View>
                </Content>
            </Container>
        );  
    }
}
