/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList, ScrollView} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import DrawerHeader from "../../Component/Header/DrawerHeader";
import {Colors} from "../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import PageNavigation from '../../Component/PageNavigation';

@inject('store')
@observer
export default class MRScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Management',
        icon: "contacts",
        permissionKey: 'mr'
    };

    async componentDidMount(){
    
    }

    render() {
        const {user_permission} = this.props.store;
        const menu = [{
            name : 'Temuan',
            iconColor : '#0288d1',
            icon : 'search',
            backgroundColor : 'rgba(2, 136, 209, 0.2)',
            route: 'ManagementQC',
            permissionKey : 'quality_control'
        },{
            name : 'Penilaian',
            iconColor : '#00c853',
            icon : 'clipboard',
            backgroundColor : 'rgba(0, 200, 83, 0.2)',
            route : 'EmployeeScore',
            permissionKey : 'employee_score'
        },{
            name : 'Pengawasan',
            iconColor : '#9c27b0',
            icon : 'eye',
            backgroundColor : 'rgba(156, 39, 176, 0.2)',
            route : 'ManagementSupervision',
            permissionKey : 'project_supervision'
        },
        {
            name : 'Notulen',
            iconColor : '#b71c1c',
            icon : 'book',
            backgroundColor : 'rgba(183, 28, 28, 0.2)',
            route : 'Notulen',
            permissionKey : 'minutes_of_meetings'
        },
        // {
        //     name : 'Rekomendasi',
        //     iconColor : '#b71c1c',
        //     icon : 'copy',
        //     backgroundColor : 'rgba(183, 28, 28, 0.2)',
        //     route : 'Recommendation'
        // },
        // {
        //     name : 'Approval Kebutuhan Project',
        //     iconColor : '#ffa000',
        //     icon : 'construct',
        //     backgroundColor : 'rgba(253, 216, 53, 0.2)',
        //     route : 'ApprovalProjectNeed'
        // },{
        //     name : 'Approval Pembiayaan',
        //     iconColor : '#4e342e',
        //     icon : 'cash',
        //     backgroundColor : 'rgba(78, 52, 46, 0.2)',
        //     route : 'ApprovalProjectCash'
        // },
        // {
        //     name : 'Notulen',
        //     iconColor : '#e91e63',
        //     icon : 'book',
        //     backgroundColor : 'rgba(233, 30, 99, 0.2)',
        //     route : 'Notulen'
        // },
        ];

        const menuPermissionKey = 'mr';
        const submenuPermission = user_permission.data.find(it=>it.key == menuPermissionKey);
        const filteredMenu = menu.filter(it=>{
            const permissionIndex = submenuPermission.submenu.findIndex(prop=>prop.key == it.permissionKey);
            return permissionIndex > -1
        });
        return (
            <ScrollView>
                <DrawerHeader title={"Management"} navigation={this.props.navigation}/>
                <View style={{padding : 20}}>
                    <PageNavigation menu={filteredMenu} navigation={this.props.navigation}/>
                </View>
            </ScrollView>
        );
    }
}
