import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import moment from 'moment';
import {startCase} from 'lodash';

@inject('store')
@observer
export default class CustomerVerificationScreen extends Component {

    async componentDidMount(){
        const {unit_akad, global_ui} = this.props.store;
        global_ui.openLoader();
        await unit_akad.getAll();
        global_ui.closeLoader();
    }

    renderItem = ({item, index})=>{
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('CustomerVerificationDetail', {
                    id : item.id
                })}>
                    <Body>
                        <Text style={[human.body, {marginBottom :10}]}>
                            {moment(item.verification_date).format('dddd, DD-MM-YYYY')}
                        </Text>
                        <Text style={[human.caption2]}>Nama Customer</Text>
                        <Text style={[human.subhead,systemWeights.light, {marginBottom :10}]}>{item.customer}</Text>
                        <Text style={[human.caption2]}>Agent</Text>
                        <Text style={[human.subhead,systemWeights.light, {marginBottom :10}]}>{(item.agent && item.agent.name) ? item.agent.name : '-'}</Text>
                        <Text style={[human.caption2]}>Unit</Text>
                        <Text style={[human.subhead,systemWeights.light, {marginBottom :10}]}>{item.unit.kavling_number}</Text>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    componentWillUnmount(){
        this.props.store.unit_akad.changeSearchData(false);
        this.props.store.guest_book.changeFilterKey('orderBy', 'verification_date');
    }

    state = {
        isFilterVisible : false
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.unit_akad.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.unit_akad.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"Verifikasi Customer"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Tanggal Verifikasi",
                        value : "verification_date"
                    },{
                        label : "Customer",
                        value : "customer"
                    }]}
                    searchByColumns={[{
                        label : "Tanggal Verifikasi",
                        value : "verification_date"
                    },{
                        label : "Customer",
                        value : "customer"
                    }]}
                    defaultOrderBy={'verification_date'}
                    defaultOrder={this.props.store.unit_akad.order}
                    defaultSearchBy={'verification_date'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1,padding : 10}}>
                    <FlatList
                        data={this.props.store.unit_akad.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
