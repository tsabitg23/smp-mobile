import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import CollapsibleCard from '../../../Component/Card/CollapsibleCard';

@inject('store')
@observer
export default class CustomerVerificationDetailScreen extends Component {

    async componentDidMount(){
        const {unit_akad, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await unit_akad.getDetail(id);
        }
        console.log(unit_akad.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {unit_akad} = this.props.store;
        const booking_date = get(unit_akad.selectedData,'booking_date',false);
        const verification_date = get(unit_akad.selectedData,'verification_date',false);
        const akad_date = get(unit_akad.selectedData,'akad_date',false);

        const infoCardData = [{
            label : 'Customer',
            value : get(unit_akad.selectedData,'customer','-')
        },{
            label : 'Nomor Kavling Unit',
            value : get(unit_akad.selectedData,'unit.kavling_number','-')
        },{
            label : 'Status Unit',
            value : get(unit_akad.selectedData,'unit.status','-')
        },{
            label : 'Agent',
            value : get(unit_akad.selectedData,'agent.name','-')
        },{
            label : 'Tanggal Booking',
            value : booking_date ? moment(booking_date).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Tanggal Verifikasi',
            value : verification_date ? moment(verification_date).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Tanggal Akad',
            value : akad_date ? moment(akad_date).format('dddd, DD-MM-YYYY') : '-'
        }]

        const verificationCardData = [{
            label : "Nama",
            value : get(unit_akad.selectedData.verification,'name','-')
        },{
            label : "Jenis Kelamin",
            value : get(unit_akad.selectedData.verification,'gender','-')
        },{
            label : "Nomor KTP",
            value : get(unit_akad.selectedData.verification,'ktp_number','-')
        },{
            label : "Kota",
            value : get(unit_akad.selectedData.verification,'city','-')
        },{
            label : "Pekerjaan",
            value : get(unit_akad.selectedData.verification,'occupation','-')
        },{
            label : 'Tanggal Verifikasi',
            value : verification_date ? moment(verification_date).format('dddd, DD-MM-YYYY') : '-'
        },];

        const bagian1Categories = get(unit_akad.selectedData.verification,'scores[0].data',[]);

        const categoriesScore = bagian1Categories.map(it=>{
            return {
                label : startCase(it.category),
                value : it.value + ` (${it.desc})`
            }
        });

        const bagian1CardData = [
        ...categoriesScore,
        {
            label : "Total Skor",
            value : get(unit_akad.selectedData.verification,'scores[0].total_score','-')
        },{
            label : "Kategori",
            value : get(unit_akad.selectedData.verification,'scores[0].category','-')
        },{
            label : "Deskripsi",
            value : get(unit_akad.selectedData.verification,'scores[0].desc','-')
        }];

        const bagian2CardData = [{
            label : "Total Skor",
            value : get(unit_akad.selectedData.verification,'scores[1].total_score','-')
        },{
            label : "Kategori",
            value : get(unit_akad.selectedData.verification,'scores[1].category','-')
        },{
            label : "Deskripsi",
            value : get(unit_akad.selectedData.verification,'scores[1].desc','-')
        }];
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Unit Verifikasi Detail"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                        <CollapsibleCard data={verificationCardData} title={"Hasil Verifikasi"}/>
                        <CollapsibleCard data={bagian1CardData} title={"Pertanyaan Bagian 1"}/>
                        <CollapsibleCard data={bagian2CardData} title={"Pertanyaan Bagian 2"}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
