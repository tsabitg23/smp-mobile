import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';

@inject('store')
@observer
export default class EmployeeDetailScreen extends Component {

    async componentDidMount(){
        const {employee, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await employee.getDetail(id);
        }
        console.log(employee.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {employee} = this.props.store;
        const join_date = get(employee.selectedData,'join_date',false);
        const infoCardData = [{
            label : 'Nama',
            value : get(employee.selectedData,'name','-')
        },{
            label : 'NIK',
            value : get(employee.selectedData,'nik','-')
        },{
            label : 'Tanggal Bergabung',
            value : join_date ? moment(join_date).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Jabatan',
            value : get(employee.selectedData,'position','-')
        },{
            label : 'Bagian/Divisi',
            value : get(employee.selectedData,'division','-') || '-'
        }]
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Detail Pegawai"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
