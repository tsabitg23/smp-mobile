import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem, Button } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import Collapsible from 'react-native-collapsible';
import CollapsibleCard from '../../../Component/Card/CollapsibleCard';

@inject('store')
@observer
export default class CustomerDetailScreen extends Component {

    async componentDidMount(){
        const {customer, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await customer.getDetail(id);
        }
        console.log(customer.selectedData)
        global_ui.closeLoader();
    }

    renderItem = ({item, index})=>{
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('UnitDetail', {
                    id : item.unit_id
                })}>
                    <Body>
                        <Text style={[human.body, {marginBottom :10}]}>
                            {item.unit.kavling_number}
                        </Text>
                        <Text style={[human.caption2]}>Tipe Pembayaran</Text>
                        <Text style={[human.subhead,systemWeights.light, {marginBottom :10}]}>{item.payment_type.name}</Text>
                        <Text style={[human.caption2]}>Downpayment</Text>
                        <Text style={[human.subhead,systemWeights.light, {marginBottom :10}]}>Rp. {item.downpayment}</Text>
                        <Text style={[human.caption2]}>Cicilan Perbulan</Text>
                        <Text style={[human.subhead,systemWeights.light, {marginBottom :10}]}>Rp. {item.installment_per_month}</Text>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        const {customer} = this.props.store;
        const dob = get(customer.selectedData,'dob',false);
        const gender = get(customer.selectedData,'gender',false);
        const id_card_type = get(customer.selectedData,'id_card_type','-');
        const infoCardData = [{
            label : 'Nama',
            value : get(customer.selectedData,'name','-')
        },{
            label : 'Email',
            value : get(customer.selectedData,'email','-')
        },{
            label : 'Jenis Kelamin',
            value : gender == 'male' ? 'Laki-laki' : 'Perempuan'
        },{
            label : 'Tempat Lahir',
            value : get(customer.selectedData,'birthplace','-')
        },{
            label : 'Tanggal Lahir',
            value : dob ? moment(dob).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Kartu Identitas',
            value : id_card_type
        },{
            label : 'Nomor ' + id_card_type,
            value : get(customer.selectedData,'id_card_number','-')
        },{
            label : 'Status Perkawinan',
            value : startCase(get(customer.selectedData,'marital_status','-'))
        },{
            label : 'Agama',
            value : get(customer.selectedData,'religion','-')
        },{
            label : 'Pendidikan Terakhir',
            value : get(customer.selectedData,'last_degree','-')
        },{
            label : 'Alamat Rumah',
            value : get(customer.selectedData,'home_address','-')
        },{
            label : 'Status Kepemilikan Rumah',
            value : get(customer.selectedData,'home_status','-')
        }]

        const phones_data = JSON.parse(get(customer.selectedData,'phones_data',"{}"));
        const contactCardData = [{
            label : "Telepon Rumah",
            value : get(phones_data,'home','-')
        },{
            label : "Handphone",
            value : get(phones_data,'handphone','-')
        },{
            label : "Whatsapp",
            value : get(phones_data,'whatsapp','-')
        },];

        const company_data = JSON.parse(get(customer.selectedData,'company_data',"{}"));
        const companyCardData = [{
            label : "Pekerjaan",
            value : get(company_data,'occupation','-')
        },{
            label : "Nama Perusahaan",
            value : get(company_data,'company_name','-')
        },{
            label : "Bidang Perusahaan",
            value : get(company_data,'company_field','-')
        },{
            label : "Alamat Kantor",
            value : get(company_data,'company_address','-')
        },{
            label : "Telepon Kantor",
            value : get(company_data,'company_phone','-')
        },{
            label : "Lama Bekerja",
            value : get(company_data,'work_duration','-')
        },];

        const contactable_data = JSON.parse(get(customer.selectedData,'contactable_data',"{}"));
        const contactableCardData = [{
            label : "Nama Kerabat",
            value : get(contactable_data,'name','-')
        },{
            label : "Hubungan",
            value : get(contactable_data,'relationship','-')
        },{
            label : "Telepon Rumah",
            value : get(contactable_data,'phone_home','-')
        },{
            label : "Handphone",
            value : get(contactable_data,'handphone','-')
        },{
            label : "Whatsapp",
            value : get(contactable_data,'whatsapp','-')
        },{
            label : "Alamat Surat Menyurat",
            value : get(contactable_data,'mail_address','-')
        },{
            label : "Status Alamat",
            value : get(contactable_data,'address_status','-')
        },{
            label : "Alamat",
            value : get(contactable_data,'address','-')
        },];

        const financial_data = JSON.parse(get(customer.selectedData,'financial_data',"{}"));
        const financialCardData = [{
            label : "Pendapatan Per Bulan",
            value : get(financial_data,'monthly_income','-')
        },{
            label : "Pendapatan Pasangan Per Bulan",
            value : get(financial_data,'spouse_monthly_income','-')
        },{
            label : "Pengeluaran Per Bulan",
            value : get(financial_data,'monthly_expense','-')
        },{
            label : "Angsuran Lain. terbayar",
            value : get(financial_data,"other_installment_pay",'-')
        },{
            label : "Angsuran Lain. Dari",
            value : get(financial_data,"other_installment_total",'-')
        },{
            label : "Tanggungan",
            value : get(financial_data,"dependents",'-')
        },];

        const customerUnits = get(customer.selectedData,'units',[]);

        return (
            <Container>
                <Content>
                    <ModalHeader title={"Detail Customer"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                        <CollapsibleCard data={contactCardData} title={"Kontak"}/>
                        <CollapsibleCard data={companyCardData} title={"Pekerjaan"}/>
                        <CollapsibleCard data={contactableCardData} title={"Kontak Kerabat"}/>
                        <CollapsibleCard data={financialCardData} title={"Finansial"}/>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Unit Customer</Text>
                        </View>
                        <FlatList
                            data={customerUnits}
                            renderItem={this.renderItem}
                            ListEmptyComponent={this.renderEmpty}
                            keyExtractor={(item)=>item.id}
                        />
                    </View>
                </Content>
            </Container>
        );  
    }
}
