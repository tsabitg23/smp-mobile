/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import DrawerHeader from "../../Component/Header/DrawerHeader";
import {Colors} from "../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import PageNavigation from '../../Component/PageNavigation';

@inject('store')
@observer
export default class HRDScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'HRD',
        icon: "briefcase",
        permissionKey: 'hrd'
    };

    async componentDidMount(){
    
    }

    render() {
        const {user_permission} = this.props.store;
        const menu = [{
            name : 'Customer',
            iconColor : '#0288d1',
            icon : 'home',
            backgroundColor : 'rgba(2, 136, 209, 0.2)',
            route: 'Customer',
            permissionKey: 'customers'
        },{
            name : 'Pegawai',
            iconColor : '#00c853',
            icon : 'people',
            backgroundColor : 'rgba(0, 200, 83, 0.2)',
            route : 'Employee',
            permissionKey: 'employees'
        },{
            name : 'Verifikasi Customer',
            iconColor : '#9c27b0',
            icon : 'person',
            backgroundColor : 'rgba(156, 39, 176, 0.2)',
            route : 'CustomerVerification',
            permissionKey: 'customer_verification'
        }]

        const menuPermissionKey = 'hrd';
        const submenuPermission = user_permission.data.find(it=>it.key == menuPermissionKey);
        const filteredMenu = menu.filter(it=>{
            const permissionIndex = submenuPermission.submenu.findIndex(prop=>prop.key == it.permissionKey);
            return permissionIndex > -1
        });
        return (
            <View>
                <DrawerHeader title={"HRD"} navigation={this.props.navigation}/>
                <View style={{padding : 20}}>
                    <PageNavigation menu={filteredMenu} navigation={this.props.navigation}/>
                </View>
            </View>
        );
    }
}
