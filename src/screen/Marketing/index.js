/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import DrawerHeader from "../../Component/Header/DrawerHeader";
import {Colors} from "../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import PageNavigation from '../../Component/PageNavigation';

@inject('store')
@observer
export default class MarketingScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Marketing',
        icon: "bookmarks",
        permissionKey: 'marketing'
    };

    async componentDidMount(){
    
    }

    render() {
        const {user_permission} = this.props.store
        const menu = [{
            name : 'Unit',
            iconColor : '#0288d1',
            icon : 'home',
            backgroundColor : 'rgba(2, 136, 209, 0.2)',
            route: 'Unit',
            permissionKey: 'units'
        },{
            name : 'Agency',
            iconColor : '#00c853',
            icon : 'people',
            backgroundColor : 'rgba(0, 200, 83, 0.2)',
            route : 'Agency',
            permissionKey: 'agencies'
        },{
            name : 'Agent',
            iconColor : '#9c27b0',
            icon : 'person',
            backgroundColor : 'rgba(156, 39, 176, 0.2)',
            route : 'Agent',
            permissionKey: 'agents'
        },{
            name : 'Buku Tamu',
            iconColor : '#b71c1c',
            icon : 'book',
            backgroundColor : 'rgba(183, 28, 28, 0.2)',
            route : 'GuestBook',
            permissionKey: 'guest_book'
        },{
            name : 'Marketing Plan',
            iconColor : '#ffa000',
            icon : 'clipboard',
            backgroundColor : 'rgba(253, 216, 53, 0.2)',
            route : 'MarketingPlan',
            permissionKey: 'marketing_plans'
        },]

        const menuPermissionKey = 'marketing';
        const submenuPermission = user_permission.data.find(it=>it.key == menuPermissionKey);
        const filteredMenu = menu.filter(it=>{
            const permissionIndex = submenuPermission.submenu.findIndex(prop=>prop.key == it.permissionKey);
            return permissionIndex > -1
        });
        return (
            <View>
                <DrawerHeader title={"Marketing"} navigation={this.props.navigation}/>
                <View style={{padding : 20}}>
                    <PageNavigation menu={filteredMenu} navigation={this.props.navigation}/>
                </View>
            </View>
        );
    }
}
