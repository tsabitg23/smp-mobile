import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import moment from 'moment';

@inject('store')
@observer
export default class MarketingPlanScreen extends Component {

    async componentDidMount(){
        const {marketing_plan, global_ui} = this.props.store;
        global_ui.openLoader();
        await marketing_plan.getAll();
        global_ui.closeLoader();
    }

    renderItem = ({item, index})=>{
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('MarketingPlanDetail', {
                    id : item.id
                })}>
                    <Body>
                        <Text style={[human.body]}>
                            {moment(item.date).format('dddd, DD-MM-YYYY')}
                        </Text>
                        <Text style={[human.footnote, systemWeights.light]}>
                            {item.type}
                        </Text>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    componentWillUnmount(){
        this.props.store.marketing_plan.changeSearchData(false);
        this.props.store.marketing_plan.changeFilterKey('orderBy', 'date');
    }

    state = {
        isFilterVisible : false
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.marketing_plan.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.marketing_plan.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"Buku Tamu"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Tanggal",
                        value : "date"
                    },{
                        label : "Tipe",
                        value : "type"
                    }]}
                    searchByColumns={[{
                        label : "Tanggal",
                        value : "date"
                    }]}
                    defaultOrderBy={'date'}
                    defaultOrder={this.props.store.marketing_plan.order}
                    defaultSearchBy={'date'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1,padding : 10}}>
                    <FlatList
                        data={this.props.store.marketing_plan.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
