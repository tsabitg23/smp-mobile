import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';

@inject('store')
@observer
export default class MarketingPlanDetailScreen extends Component {

    async componentDidMount(){
        const {marketing_plan, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await marketing_plan.getDetail(id);
        }
        console.log(marketing_plan.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {marketing_plan} = this.props.store;
        const date = get(marketing_plan.selectedData,'date',false);
        const infoCardData = [{
            label : 'Rencana Tanggal',
            value : date ? moment(date).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Penanggung Jawab',
            value : get(marketing_plan.selectedData,'employee.name','-')
        },{
            label : 'Tipe',
            value : get(marketing_plan.selectedData,'type','-')
        },{
            label : 'Status',
            value : get(marketing_plan.selectedData,'status','-')
        }]
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Marketing Plan Detail"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
