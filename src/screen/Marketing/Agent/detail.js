import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';

@inject('store')
@observer
export default class AgentDetailScreen extends Component {

    async componentDidMount(){
        const {agent, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await agent.getDetail(id);
        }
        console.log(agent.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {agent} = this.props.store;
        const join_date = get(agent.selectedData,'join_date',false);
        const infoCardData = [{
            label : 'Nama',
            value : get(agent.selectedData,'name','-')
        },{
            label : 'Nama Agency',
            value : get(agent.selectedData,'agency.name','-')
        },{
            label : 'Email',
            value : get(agent.selectedData,'email','-')
        },{
            label : 'Telepon',
            value : get(agent.selectedData,'phone','-')
        },{
            label : 'Tanggal Bergabung',
            value : join_date ? moment(join_date).format('dddd, DD-MM-YYYY') : '-'
        }]
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Agent Detail"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
