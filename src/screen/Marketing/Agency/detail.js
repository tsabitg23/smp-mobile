import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
@inject('store')
@observer
export default class AgencyDetailScreen extends Component {

    async componentDidMount(){
        const {agency, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await agency.getDetail(id);
        }
        console.log(agency.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {agency} = this.props.store;
        const agent = get(agency.selectedData,'agents',[]);
        const infoCardData = [{
            label : 'Nama',
            value : get(agency.selectedData,'name','-')
        },{
            label : 'Telepon',
            value : get(agency.selectedData,'phone','-')
        },{
            label : 'Alamat',
            value : get(agency.selectedData,'address','-')
        },{
            label : 'Jumlah Agent',
            value : agent.length
        }]
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Agency Detail"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
