import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';
import { getStatusColor } from '../../../utils/styles';

@inject('store')
@observer
export default class UnitDetailScreen extends Component {

    async componentDidMount(){
        const {unit, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await unit.getDetail(id);
        }
        console.log(unit.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {unit} = this.props.store;
        const status = get(unit.selectedData,'status','-');
        const infoCardData = [{
            label : 'Nomor Kavling',
            value : get(unit.selectedData,'kavling_number','-')
        },{
            label : 'Luas Tanah',
            value : get(unit.selectedData,'land_area','-')
        },{
            label : 'Luas Bangunan',
            value : get(unit.selectedData,'building_area','-')
        },{
            label : 'Status',
            value : startCase(status),
            valueColor : getStatusColor(status)
        }]
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Unit Detail"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
