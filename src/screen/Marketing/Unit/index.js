import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import moment from 'moment';
import {startCase} from 'lodash';

@inject('store')
@observer
export default class UnitScreen extends Component {

    async componentDidMount(){
        const {unit, global_ui} = this.props.store;
        global_ui.openLoader();
        await unit.getAll();
        global_ui.closeLoader();
    }

    getColor = (status) => {
        if (status == 'sold') {
            return Colors.primary
        }
        else if (status == 'booking_fee_paid') {
            return Colors.warn
        }
        else if (status == 'available') {
            return '#3e2723'
        }
        return '#000';
    }

    renderItem = ({item, index})=>{
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('UnitDetail', {
                    id : item.id
                })}>
                    <Body>
                        <Text style={[human.body]}>
                            {item.kavling_number}
                        </Text>
                        <Text style={[human.footnote, systemWeights.semibold, {color : this.getColor(item.status)}]}>
                            {startCase(item.status)}
                        </Text>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    componentWillUnmount(){
        this.props.store.unit.changeSearchData(false);
    }

    state = {
        isFilterVisible : false
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.unit.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.unit.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"Unit"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Tanggal",
                        value : "created_at"
                    },{
                        label : "Nomor Kavling",
                        value : "kavling_number"
                    },{
                        label : "Status",
                        value : "status"
                    }]}
                    searchByColumns={[{
                        label : "Nomor Kavling",
                        value : "kavling_number"
                    }]}
                    defaultOrderBy={this.props.store.unit.orderBy}
                    defaultOrder={this.props.store.unit.order}
                    defaultSearchBy={'kavling_number'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1,padding : 10}}>
                    <FlatList
                        data={this.props.store.unit.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
