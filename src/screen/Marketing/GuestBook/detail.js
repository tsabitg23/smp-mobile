import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import ModalHeader from '../../../Component/Header/ModalHeader';
import InfoCard from '../../../Component/Card/InfoCard';
import {get, startCase} from 'lodash';
import moment from 'moment';

@inject('store')
@observer
export default class GuestBookDetailScreen extends Component {

    async componentDidMount(){
        const {guest_book, global_ui} = this.props.store;
        const id = this.props.navigation.getParam('id', false);
        global_ui.openLoader();
        if(id){
            await guest_book.getDetail(id);
        }
        console.log(guest_book.selectedData)
        global_ui.closeLoader();
    }

    render() {
        const {guest_book} = this.props.store;
        const survei_date = get(guest_book.selectedData,'survei_date',false);
        const infoCardData = [{
            label : 'Tanggal Survei',
            value : survei_date ? moment(survei_date).format('dddd, DD-MM-YYYY') : '-'
        },{
            label : 'Nama',
            value : get(guest_book.selectedData,'name','-')
        },{
            label : 'Telepon',
            value : get(guest_book.selectedData,'phone','-')
        },{
            label : 'Alamat',
            value : get(guest_book.selectedData,'address','-')
        },{
            label : 'Sumber Info',
            value : get(guest_book.selectedData,'info_source','-')
        }]
        return (
            <Container>
                <Content>
                    <ModalHeader title={"Buku Tamu"} onClickBack={()=>this.props.navigation.goBack()}/>
                    <View style={{flex:1,padding : 10}}>
                        <View>
                            <Text style={[human.footnote, systemWeights.regular,{color: Colors.passive, paddingTop:10, marginBottom : 10}]}>Data</Text>
                        </View>
                        <InfoCard data={infoCardData}/>
                    </View>
                </Content>
            </Container>
        );
    }
}
