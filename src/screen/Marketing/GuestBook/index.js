import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import {Colors} from "../../../config/variables";
import {inject, observer} from 'mobx-react';
import { human, systemWeights  } from 'react-native-typography';
import ComponentHeader from '../../../Component/Header/ComponentHeader';
import FilterModal from '../../../Component/Modal/FilterModal';
import EmptyCard from '../../../Component/EmptyCard';
import moment from 'moment';

@inject('store')
@observer
export default class GuestBookScreen extends Component {

    async componentDidMount(){
        const {guest_book, global_ui} = this.props.store;
        global_ui.openLoader();
        await guest_book.getAll();
        global_ui.closeLoader();
    }

    renderItem = ({item, index})=>{
        return (
            <Card style={{borderRadius: 5}} >
                <CardItem button onPress={()=>this.props.navigation.navigate('GuestBookDetail', {
                    id : item.id
                })}>
                    <Body>
                        <Text style={[human.body]}>
                            {moment(item.survei_date).format('dddd, DD-MM-YYYY')}
                        </Text>
                        <Text style={[human.footnote, systemWeights.light]}>
                            {item.name}
                        </Text>
                        <Text style={[human.footnote, systemWeights.light]}>
                            {item.phone}
                        </Text>
                    </Body>
                </CardItem>
            </Card>
        )
    }

    componentWillUnmount(){
        this.props.store.guest_book.changeSearchData(false);
        this.props.store.guest_book.changeFilterKey('orderBy', 'survei_date');
    }

    state = {
        isFilterVisible : false
    }

    toggleFilter = ()=>{
        this.setState({
            isFilterVisible : !this.state.isFilterVisible
        })
    }

    closeFilterModal = ()=>{
        this.setState({
            isFilterVisible : false
        })
    }

    changeDataFilter = (key, val)=>{
        this.props.store.guest_book.changeFilterKey(key, val)
    }

    filterOkClicked = (data)=>{
        this.props.store.guest_book.changeSearchData(data.isSearching, data.searchBy, data.searchKeyword);
        this.setState({
            isFilterVisible : false
        })
    }

    renderEmpty = () => {
        return <EmptyCard title={"Kosong ..."} subtitle={"Data tidak ditemukan"}/>
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ComponentHeader title={"Buku Tamu"} navigation={this.props.navigation} onClickFilter={()=>this.toggleFilter()}/>
                <FilterModal 
                    visible={this.state.isFilterVisible} 
                    onCloseModal={()=>this.closeFilterModal()} 
                    onChangeFilterValue={this.changeDataFilter}
                    orderByColumns={[{
                        label : "Tanggal",
                        value : "survei_date"
                    },{
                        label : "Nama",
                        value : "name"
                    },{
                        label : "Telepon",
                        value : "phone_number"
                    }]}
                    searchByColumns={[{
                        label : "Tanggal",
                        value : "survei_date"
                    },{
                        label : "Nama",
                        value : "name"
                    },{
                        label : "Telepon",
                        value : "phone"
                    }]}
                    defaultOrderBy={'survei_date'}
                    defaultOrder={this.props.store.guest_book.order}
                    defaultSearchBy={'survei_date'}
                    onOkClicked={(res)=>this.filterOkClicked(res)}
                />
                <View style={{flex:1,padding : 10}}>
                    <FlatList
                        data={this.props.store.guest_book.getData}
                        renderItem={this.renderItem}
                        ListEmptyComponent={this.renderEmpty}
                        keyExtractor={(item)=>item.id}/>
                </View>
            </View>
        );
    }
}
