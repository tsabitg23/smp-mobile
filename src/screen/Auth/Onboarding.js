import React from 'react';
import {
    View,
    ActivityIndicator
} from 'react-native'
import {Colors} from "../../config/variables";
import AsyncStorage from '@react-native-community/async-storage';
export default class OnBoarding extends React.Component{
    componentDidMount(){
        this.checkLogin()
    }

    checkLogin = async ()=>{
        try {
            const value = await AsyncStorage.getItem('@token');
            if(value !== null) {
                this.props.navigation.navigate('Home')
            }else{
                this.props.navigation.navigate('Login')
            }
        } catch(e) {
            // error reading value
        }
    };

    render(){
        return (
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <ActivityIndicator size={"large"} color={Colors.primary}/>
            </View>
        )
    }
}