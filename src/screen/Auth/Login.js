import React from 'react';
import {
    View,
    Text,
    Alert, Image
} from 'react-native'
import {
    Button,
    Form,
    Item,
    Input,
    Label
} from 'native-base'
import {Colors} from "../../config/variables";
import { human } from 'react-native-typography'
import {inject, observer} from 'mobx-react';

@inject('store')
@observer
export default class Login extends React.Component{
    state = {
        email : '',
        password : ''
    };

    handleChange = (key,value)=>{
        this.setState({
            [key] : value
        })
    };

    login = ()=>{
        const {email,password} = this.state;
        const {auth, global_ui} = this.props.store;
        if(!email || !password){
            return Alert.alert('Login gagal', 'Harap isi form dengan benar')
        }

        global_ui.openLoader();
        auth.login(email, password).then(res=>{
            global_ui.closeLoader();
            this.props.navigation.navigate('Home');
        }).catch(err=>{
            console.log(err);
            global_ui.closeLoader();
            return Alert.alert('Login gagal', 'Harap masukan email dan password dengan benar')
        })
    };

    render(){
        return (
            <View style={{backgroundColor:Colors.whiteGrey, flex:1,flexDirection:'column-reverse', alignItems:'center', padding : 30, justifyContent:'center'}}>
                <Button full style={{backgroundColor : Colors.primary}} rounded onPress={this.login}>
                    <Text style={[human.body,{color: '#FFF'}]}>MASUK</Text>
                </Button>
                <View style={{backgroundColor : Colors.white, width: '100%', marginBottom: 20, borderRadius: 10}}>
                    <Form>
                        <Item>
                            <Input placeholder={"Username"} value={this.state.email} onChangeText={(text)=>this.handleChange('email',text)} autoCapitalize={"none"}/>
                        </Item>
                        <Item style={{borderColor: 'transparent'}}>
                            <Input placeholder={"Password"} secureTextEntry={true} value={this.state.password} onChangeText={(text)=>this.handleChange('password',text)}/>
                        </Item>
                    </Form>
                </View>
                <View>
                    <Image source={require('../../assets/rumahdps-logo2.jpeg')} resizeMode={"contain"} style={{width: 200}}/>
                </View>
            </View>
        )
    }
}