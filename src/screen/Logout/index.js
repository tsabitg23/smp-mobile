import React from 'react';
import {View, Text} from 'react-native';
import {  Icon } from 'native-base';
import {Colors} from "../../config/variables";
import AsyncStorage from '@react-native-community/async-storage';
import {inject, observer} from 'mobx-react';

@inject('store')
@observer
export default class Logout extends React.Component{
    static navigationOptions = {
        drawerLabel: 'Logout',
        icon: "exit",
        permissionKey : 'for_all'
    };

    componentDidMount(){
        this.logout();
    }

    logout = async ()=>{
        try {
            this.props.store.setToken(null);
            const value = await AsyncStorage.removeItem('@token');
            this.props.navigation.navigate("Login")
        } catch(e) {
            // error reading value
        }
    };

    render(){
        return (
            <View />
        )
    }
}