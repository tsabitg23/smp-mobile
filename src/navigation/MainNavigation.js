import React from "react";
import { createStackNavigator, createDrawerNavigator } from "react-navigation";
import HomeScreen from "../screen/Home";
import Logout from "../screen/Logout";
import DrawerComponent from '../Component/Drawer/DrawerCustomComponent';
import Project from "../screen/Admin/Project/index";
import AdminScreen from "../screen/Admin";
import MarketingScreen from "../screen/Marketing";
import HRDScreen from "../screen/HRD";
import SupervisorScreen from "../screen/Supervisor";
import MRScreen from "../screen/MR";
import UserScreen from "../screen/Admin/User";
import UserDetailScreen from "../screen/Admin/User/Detail";
import NotulenScreen from "../screen/MR/Notulen";
import NotulenDetailScreen from "../screen/MR/Notulen/Detail";
import AgencyScreen from "../screen/Marketing/Agency";
import AgencyDetailScreen from "../screen/Marketing/Agency/detail";
import AgentScreen from "../screen/Marketing/Agent";
import AgentDetailScreen from "../screen/Marketing/Agent/detail";
import GuestBookScreen from "../screen/Marketing/GuestBook";
import GuestBookDetailScreen from "../screen/Marketing/GuestBook/detail";
import MarketingPlanScreen from "../screen/Marketing/MarketingPlan";
import MarketingPlanDetailScreen from "../screen/Marketing/MarketingPlan/detail";
import UnitScreen from "../screen/Marketing/Unit";
import UnitDetailScreen from "../screen/Marketing/Unit/detail";
import EmployeeScreen from "../screen/HRD/Employee";
import EmployeeDetailScreen from "../screen/HRD/Employee/detail";
import CustomerScreen from "../screen/HRD/Customer";
import CustomerDetailScreen from "../screen/HRD/Customer/detail";
import CustomerVerificationScreen from "../screen/HRD/VerifikasiCustomer";
import CustomerVerificationDetailScreen from "../screen/HRD/VerifikasiCustomer/detail";
import ReportScreen from "../screen/Supervisor/Report";
import ReportDetailScreen from "../screen/Supervisor/Report/detail";
import ProjectTimelineScreen from "../screen/Supervisor/ProjectTimeline";
import ProjectTimelineDetailScreen from "../screen/Supervisor/ProjectTimeline/detail";
import UnitProgressScreen from "../screen/Supervisor/UnitProgress";
import UnitProgressDetailScreen from "../screen/Supervisor/UnitProgress/detail";
import ProjectPlanningScreen from "../screen/Supervisor/PengawasanProject";
import ProjectPlanningDetailScreen from "../screen/Supervisor/PengawasanProject/detail";
import ProjectJobScreen from "../screen/Supervisor/KebutuhanProject";
import ProjectJobDetailScreen from "../screen/Supervisor/KebutuhanProject/detail";
import ProjectPaymentRequestScreen from "../screen/Supervisor/PembiayaanProject";
import ProjectPaymentRequestDetailScreen from "../screen/Supervisor/PembiayaanProject/detail";
import ProjectQualityControlScreen from "../screen/Supervisor/QualityControl";
import ProjectQualityControlDetailScreen from "../screen/Supervisor/QualityControl/detail";
import MRQualityControlScreen from "../screen/MR/Temuan";
import MRQualityControlDetailScreen from "../screen/MR/Temuan/detail";
import ManagementSupervisionScreen from "../screen/MR/Pengawasan";
import ManagementSupervisionDetailScreen from "../screen/MR/Pengawasan/detail";
import EmployeeScoreScreen from "../screen/MR/Penilaian";
import EmployeeScoreDetailScreen from "../screen/MR/Penilaian/detail";

const drawer = createDrawerNavigator({
    Home: {
        screen: HomeScreen
    },
    Admin : {
        screen : AdminScreen
    },
    Marketing : {
        screen : MarketingScreen
    },
    HRD : {
        screen : HRDScreen
    },
    Supervisor : {
        screen : SupervisorScreen
    },
    Management : {
        screen : MRScreen
    },
    Logout : {
        screen : Logout
    }
}, {
    contentComponent : DrawerComponent
})

export default createStackNavigator({
    Drawer : drawer,
    Project : {
        screen : Project
    },
    User : {
        screen : UserScreen
    },
    UserDetail : {
        screen : UserDetailScreen
    },
    Notulen : {
        screen : NotulenScreen
    },
    NotulenDetail : {
        screen : NotulenDetailScreen
    },
    Agency : {
        screen: AgencyScreen
    },
    AgencyDetail : {
        screen : AgencyDetailScreen
    },
    Agent: {
        screen : AgentScreen
    },
    AgentDetail : {
        screen : AgentDetailScreen
    },
    GuestBook : {
        screen : GuestBookScreen
    },
    GuestBookDetail : {
        screen : GuestBookDetailScreen
    },
    MarketingPlan : {
        screen : MarketingPlanScreen
    },
    MarketingPlanDetail : {
        screen : MarketingPlanDetailScreen
    },
    Unit : {
        screen : UnitScreen
    },
    UnitDetail : {
        screen : UnitDetailScreen
    },
    Employee : {
        screen : EmployeeScreen
    },
    EmployeeDetail : {
        screen : EmployeeDetailScreen
    },
    Customer : {
        screen : CustomerScreen
    },
    CustomerDetail : {
        screen : CustomerDetailScreen
    },
    CustomerVerification : {
        screen : CustomerVerificationScreen
    },
    CustomerVerificationDetail : {
        screen : CustomerVerificationDetailScreen
    },
    Report : {
        screen : ReportScreen
    },
    ReportDetail : {
        screen : ReportDetailScreen
    },
    ProjectTimeline : {
        screen : ProjectTimelineScreen
    },
    ProjectTimelineDetail : {
        screen : ProjectTimelineDetailScreen
    },
    UnitProgress : {
        screen : UnitProgressScreen
    },
    UnitProgressDetail : {
        screen : UnitProgressDetailScreen
    },
    ProjectSupervisor : {
        screen : ProjectPlanningScreen
    },
    ProjectSupervisorDetail : {
        screen : ProjectPlanningDetailScreen
    },
    ProjectJob : {
        screen : ProjectJobScreen
    },
    ProjectJobDetail : {
        screen : ProjectJobDetailScreen
    },
    ProjectPaymentRequest : {
        screen : ProjectPaymentRequestScreen
    },
    ProjectPaymentRequestDetail : {
        screen : ProjectPaymentRequestDetailScreen
    },
    QualityControl : {
        screen : ProjectQualityControlScreen
    },
    QualityControlDetail : {
        screen : ProjectQualityControlDetailScreen
    },
    ManagementQC : {
        screen : MRQualityControlScreen
    },
    ManagementQCDetail : {
        screen : MRQualityControlDetailScreen
    },
    ManagementSupervision : {
        screen : ManagementSupervisionScreen
    },
    ManagementSupervisionDetail : {
        screen : ManagementSupervisionDetailScreen
    },
    EmployeeScore : {
        screen : EmployeeScoreScreen
    },
    EmployeeScoreDetail : {
        screen : EmployeeScoreDetailScreen
    }
},{
    headerMode : 'none',
    mode: 'modal',
})