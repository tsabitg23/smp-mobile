import React from "react";
import {createAppContainer,createSwitchNavigator } from "react-navigation";
import {observer, Provider} from 'mobx-react';
import {StyleProvider } from 'native-base';
import MainStack from './MainNavigation'
import Login from "../screen/Auth/Login";
import Onboarding from "../screen/Auth/Onboarding";
import {MainStore} from "../store";
import Loader from "react-native-modal-loader";
import {Colors} from "../config/variables";
import moment from 'moment';
import 'moment/locale/id';
moment().locale('id');

const Store = new MainStore();
const AppNavigator = createSwitchNavigator({
    OnBoarding : {
        screen: Onboarding
    },
    Login : {
        screen: Login
    },
    Main: {
        screen: MainStack
    }
},
    {
        mode: 'modal',
        headerMode: 'none',
    });

const AppContainer=  createAppContainer(AppNavigator);

@observer
export default class App extends React.Component{
    render(){
        return (
            <Provider store={Store}>
                <React.Fragment>
                    <AppContainer/>
                    <Loader loading={Store.global_ui.isLoading} color={Colors.primary}/>
                </React.Fragment>
            </Provider>
        )
    }
}