export const Colors = {
    primary : '#23a950',
    primaryLight : '#2cd665',
    white: '#FFF',
    whiteGrey : '#F7F7F7',
    passive: '#757575',
    warn : '#fbc02d',
    danger : '#d50000'
};