import React from 'react';
import {View, Modal, Text, Dimensions} from 'react-native';
import { Header, Title, Left, Right, Body, Icon, Button,List, ListItem,Picker, Item, Input, Label } from 'native-base';
import {Colors} from "../../config/variables";
import { human, systemWeights  } from 'react-native-typography';
import ModalHeader from '../Header/ModalHeader';
import {get} from 'lodash';
import TitleOnlyHeader from '../Header/TitleOnly';
const {width, height} = Dimensions.get('window');
export default class FilterModal extends React.Component{
    state= {
        modalVisible : this.props.visible,
        orderBy : this.props.defaultOrderBy,
        searchBy : this.props.defaultSearchBy,
        order : this.props.defaultOrder,
        searchKeyword : ''
    };

    componentDidUpdate(prevProps) {
        let change = {};
        if (this.props.visible !== prevProps.visible) {
          change.modalVisible = this.props.visible;
        }
    
        if(Object.keys(change).length > 0){
            this.setState(change);
        }
    }

    backModal = ()=>{
        this.props.onCloseModal();
    }

    onChangeFilterValue = (key, val)=>{
        this.setState({
            [key]: val
        });
        this.props.onChangeFilterValue(key,val);
    }

    search = () => {
        this.props.onOkClicked({
            isSearching : (!!this.state.searchKeyword),
            searchKeyword : this.state.searchKeyword,
            searchBy : this.state.searchBy
        })
    }

    render(){
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    this.setState({modalVisible : false})
                }}>
                {/* <ModalHeader title="Filter" onClickBack={this.backModal}/> */}
                <TitleOnlyHeader title={"Filter"}/>
                <View style={{flex:1, paddingVertical : 20}}>
                <Text style={[human.subhead, systemWeights.semibold,{paddingLeft:10, paddingBottom : 10}]}>Urutkan</Text>                    
                <ListItem icon>
                    <Body>
                        <Text>Berdasarkan</Text>
                    </Body>
                    <Right>
                    <Picker
                        note
                        mode="dropdown"
                        style={{ width: width*50/100 }}
                        selectedValue={this.state.orderBy}
                        onValueChange={(val)=>this.onChangeFilterValue('orderBy',val)}
                    >
                        {
                            this.props.orderByColumns.map(it=>{
                                return <Picker.Item key={it.value} label={it.label} value={it.value} />
                            })
                        }
                    </Picker>
                    </Right>
                </ListItem>
                <ListItem icon>
                    <Body>
                        <Text>Urutan</Text>
                    </Body>
                    <Right>
                    <Picker
                        note
                        mode="dropdown"
                        style={{ width: width*50/100 }}
                        selectedValue={this.state.order}
                        onValueChange={(val)=>this.onChangeFilterValue('order',val)}
                    >
                        <Picker.Item label="Terkecil/Terlama" value="asc" />
                        <Picker.Item label="Terbesar/Terbaru" value="desc" />
                    </Picker>
                    </Right>
                </ListItem>
                <Text style={[human.subhead, systemWeights.semibold,{paddingLeft:10, paddingBottom : 10, paddingTop : 20}]}>Cari</Text>
                <ListItem icon>
                    <Body>
                        <Text>Berdasarkan</Text>
                    </Body>
                    <Right>
                    <Picker
                        note
                        mode="dropdown"
                        style={{ width: width*50/100 }}
                        selectedValue={this.state.searchBy}
                        onValueChange={(val)=>this.onChangeFilterValue('searchBy',val)}
                    >
                        {
                            this.props.searchByColumns.map(it=>{
                                return <Picker.Item key={it.value} label={it.label} value={it.value} />
                            })
                        }
                    </Picker>
                    </Right>
                </ListItem>
                <ListItem last>
                    <Item floatingLabel>
                        <Label style={{fontSize: 14}}>Kata kunci</Label>
                        <Input value={this.state.searchKeyword} onChangeText={(text)=>this.setState({searchKeyword : text})}/>
                    </Item>
                </ListItem>
                </View>
                <View style={{padding : 10}}>
                    <Button full style={{backgroundColor : Colors.primary, borderRadius : 5}} onPress={()=>this.search()}>
                        <Text style={[human.bodyWhite]}>OK</Text>
                    </Button>
                </View>
            </Modal>
        )
    }
}

FilterModal.defaultProps = {
    visible : false,
    onCloseModal : ()=>{},
    onChangeFilterValue : ()=>{},
    orderByColumns : [],
    defaultOrder : 'asc',
    defaultOrderBy : 'created_at',
    searchByColumns : [],
    defaultSearchBy : '',
    onOkClicked : ()=>{}, 
}