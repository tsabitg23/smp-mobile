import React from 'react'
import {FlatList} from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Text } from 'native-base';

export default class BasicList extends React.Component {
    
    render(){
        return (
            <FlatList
                data={this.props.data}
                renderItem={this.renderItem}
            />
        )
    }
}