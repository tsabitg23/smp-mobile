import React from 'react';
import { Body, Card, CardItem, Icon } from 'native-base';
import { human, systemWeights  } from 'react-native-typography';
import { Text, View } from 'react-native';
import Collapsible from 'react-native-collapsible';
import { Colors } from '../../config/variables';

export default class CollapsibleScoreCard extends React.Component {
    renderCardItem = (data) => {
        const {label, value, valueColor, isHeader}  = data;
        const valueStyle = {};
        if (valueColor) {
            valueStyle.color = valueColor;
        }
        return (
            <CardItem key={label}>
                <Body style={{flexDirection : 'row',justifyContent:'space-between', flex:1}}>
                    <Text style={[isHeader ? human.subhead : human.footnote, isHeader && systemWeights.bold, {flex: 0.8}]}>{label}</Text>
                    {value ? <Text style={[human.footnote, valueStyle, {flex: 0.2, textAlign:'center'}]}>{value}</Text> : <View></View>}
                </Body>
            </CardItem>
        )
    }

    state = {
        isCollapsed : false
    }

    render(){
        const {data} = this.props;

        return (
            <Card>
                <CardItem header button onPress={()=>this.setState({isCollapsed : !this.state.isCollapsed})}>
                    <View style={{flex:1,flexDirection : 'row', justifyContent: 'space-between'}}>
                        <Text style={[human.footnote, systemWeights.semibold]}>{this.props.title}</Text>
                        <Icon name={this.state.isCollapsed ? 'arrow-dropdown' : 'arrow-dropup'}/>
                    </View>
                </CardItem>
                <Collapsible collapsed={this.state.isCollapsed}>
                    <CardItem>
                        <Body>
                            <Text style={[human.caption2]}>{this.props.subtitle}</Text>
                        </Body>
                    </CardItem>
                    {data.map(it=>{
                        return this.renderCardItem(it)
                    })}
                </Collapsible>
            </Card>
        )
    }
}

CollapsibleScoreCard.defaultProps= {
    data : [],
    title : 'card'
}