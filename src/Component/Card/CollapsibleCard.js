import React from 'react';
import { Body, Card, CardItem, Icon } from 'native-base';
import { human, systemWeights  } from 'react-native-typography';
import { Text, View } from 'react-native';
import Collapsible from 'react-native-collapsible';
import { Colors } from '../../config/variables';

export default class CollapsibleCard extends React.Component {
    renderCardItem = (data) => {
        const {label, value, valueColor}  = data;
        const valueStyle = {};
        if (valueColor) {
            valueStyle.color = valueColor;
        }
        return (
            <CardItem key={label}>
                <Body>
                    <Text style={[human.caption2]}>{label}</Text>
                    {value && <Text style={[human.body, valueStyle]}>{value}</Text>}
                </Body>
            </CardItem>
        )
    }

    state = {
        isCollapsed : true
    }

    render(){
        const {data} = this.props;

        return (
            <Card>
                <CardItem header button onPress={()=>this.setState({isCollapsed : !this.state.isCollapsed})}>
                    <View style={{flex:1,flexDirection : 'row', justifyContent: 'space-between'}}>
                        <Text style={[human.footnote, systemWeights.semibold]}>{this.props.title}</Text>
                        <Icon name={this.state.isCollapsed ? 'arrow-dropdown' : 'arrow-dropup'}/>
                    </View>
                </CardItem>
                <Collapsible collapsed={this.state.isCollapsed}>
                    {data.map(it=>{
                        return this.renderCardItem(it)
                    })}
                </Collapsible>
            </Card>
        )
    }
}

CollapsibleCard.defaultProps= {
    data : [],
    title : 'card'
}