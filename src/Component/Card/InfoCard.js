import React from 'react';
import { Body, Card, CardItem } from 'native-base';
import { human, systemWeights  } from 'react-native-typography';
import { Text } from 'react-native';

export default class InfoCard extends React.Component {
    renderCardItem = (data) => {
        const {label, value, valueColor}  = data;
        const valueStyle = {};
        if (valueColor) {
            valueStyle.color = valueColor;
        }
        return (
            <CardItem key={label}>
                <Body>
                    <Text style={[human.caption2]}>{label}</Text>
                    <Text style={[human.body, valueStyle]}>{value}</Text>
                </Body>
            </CardItem>
        )
    }

    render(){
        const {data} = this.props;

        return (
            <Card>
                {data.map(it=>{
                    return this.renderCardItem(it)
                })}
            </Card>
        )
    }
}

InfoCard.defaultProps= {
    data : [] 
}