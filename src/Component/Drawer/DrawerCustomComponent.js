import React from 'react';
import {View, Text, FlatList, Alert, ImageBackground, Picker} from 'react-native';
import {ListItem, Left, Body, Icon } from 'native-base';
import { human, systemWeights  } from 'react-native-typography';
import { Colors } from '../../config/variables';
import {inject, observer} from 'mobx-react';
import {get, capitalize} from 'lodash';

@inject('store')
@observer
export default class DrawerComponent extends React.Component{
    state={
        selectedProject : this.props.store.userData.selected_project
    }

    componentDidMount(){
        // console.log(this.props)
    }

    onClickMenu = (key) => {
        if(key == 'Logout'){
            Alert.alert(
                'Logout',
                'Anda yakin ingin keluar?',
                [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {text: 'OK', onPress: () => this.props.navigation.navigate(key)},
                ],
                {cancelable: true},
              );
        }
        else if(key == this.props.activeItemKey){
            this.props.navigation.closeDrawer()
        }
        else{
            this.props.navigation.navigate(key)
        }
    }

    renderMenuList = ({item}) => {
        return (
            <ListItem icon onPress={()=>this.onClickMenu(item.key)}>
                <Left>
                    <Icon active name={item.options.icon} style={[{color : Colors.primary,fontSize: 16}]}/>
                </Left>
                <Body>
                    <Text style={[human.body, systemWeights.light,{fontSize: 16}]}>{item.options.drawerLabel}</Text>
                </Body>
            </ListItem>
        )
    }

    changeProject  = (val, index) => {
        const {project} = this.props.store
        this.setState({
            selectedProject : val
        });
        project.setProject(val).then(res=>{
            if(this.props.activeItemKey == 'Home'){
                this.props.navigation.closeDrawer();
            }
            else{
                this.props.navigation.navigate('Home')
            }
        });
    }

    menuString = (prop) =>{
        if(prop == 'hrd'){
            return 'HRD'
        }
        else if(prop == 'mr'){
            return 'Management'
        }
        else if(prop == 'mr'){
            return 'Management'
        }
        else{
            return capitalize(prop)
        }
    }

    render(){
        const {project, user_permission} = this.props.store;
        
        const navigationMenu = Object.keys(this.props.descriptors).map(it=>{
            return this.props.descriptors[it]
        });

        const menus = navigationMenu.filter(it=>{
            if(it.options.permissionKey == 'for_all'){
                return true
            };
            return user_permission.data.findIndex(prop=>this.menuString(prop.key) == it.key) > -1
        })

        
        return (
            <View>
                <View>
                <ImageBackground source={require('../../assets/drawer-header.jpg')} style={{width: '100%', height: 150}}>
                    <View style={{width: '100%', height: '100%', backgroundColor:Colors.primary, opacity: 0.7}}>
                    </View>
                    <View style={{position:'absolute', bottom: 20, left:10}}>
                        <Text style={[human.headline, {color: '#FFF', marginBottom: 5}]}>{get(this.props.store.userData,'email','email')}</Text>
                        <Text style={[human.caption1, {color: '#FFF'}]}>{get(this.props.store.userData,'role','role')}</Text>
                    </View>
                </ImageBackground>
                </View>
                <View style={{padding : 10}}>
                    <Text style={[human.caption1,{color:Colors.passive}]}>Pilih Project</Text>
                    <Picker
                        selectedValue={this.state.selectedProject}
                        style={{height: 50, width: '100%'}}
                        onValueChange={this.changeProject}>
                        {
                            project.data.length > 0 ? project.data.map(it=>{
                                return (
                                    <Picker.Item key={it.id} label={it.name} value={it.id} />
                                )
                            })

                            :
                            <Picker.Item label={'not found'} value={''} enabled={false}/>
                        }
                    </Picker>
                </View>
                <View>
                    <FlatList
                        data={menus}
                        renderItem={this.renderMenuList}
                    />
                </View>
            </View>
        )
    }
}