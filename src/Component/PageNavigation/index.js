import React from 'react';
import {View, Dimensions, Text, TouchableOpacity} from 'react-native';
import {Icon} from 'native-base';
import { human, systemWeights  } from 'react-native-typography';
import { globalStyle } from '../../utils/styles';
const {height, width} = Dimensions.get('window');
export default class PageNavigation extends React.Component {
    
    goToPage = (route)=>{
        if(route){
            this.props.navigation.navigate(route)
        }
    }
    _renderMenu = (data) => {
        return (
            <TouchableOpacity onPress={()=>this.goToPage(data.route)} key={data.name} 
                style={
                    [globalStyle.card,
                    {
                        width : width * 40/100, 
                        height: width * 40/100, 
                        display: 'flex', 
                        alignItems:'center', 
                        justifyContent:'center', 
                        flexDirection: 'column', 
                        marginBottom : 20,
                        padding:10
                    }]}>
                <View style={{display: 'flex', alignItems:'center', justifyContent:'center', width : 64, height : 64, borderRadius:50, backgroundColor: data.backgroundColor, marginBottom: 10}}>
                    <Icon name={data.icon} style={{color : data.iconColor, fontSize:32}}/>
                </View>
                <Text style={[human.body, systemWeights.light,{textAlign:'center'}]}>{data.name}</Text>
            </TouchableOpacity>
        )
    }
    
    render(){
        const {menu} = this.props;

        return (
            <View style={{flex:1, flexDirection : 'row', flexWrap: 'wrap',justifyContent:'space-around'}}>
                {menu.map(it=>{
                    return this._renderMenu(it)
                })}
            </View>
        )
    }
} 

PageNavigation.defaultProps = {
    menu : []
}