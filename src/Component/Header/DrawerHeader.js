import React from 'react';
import {View} from 'react-native';
import { Header, Title, Left, Right, Body, Icon, Button, Text } from 'native-base';
import {Colors} from "../../config/variables";
import { human, systemWeights  } from 'react-native-typography';

export default class DrawerHeader extends React.Component{
    openMenu = ()=>{
        this.props.navigation.openDrawer();
    };

    render(){
        // return (
        //     <Header style={{backgroundColor : Colors.primary}}>
        //         <Left>
        //             <Button transparent onPress={this.openMenu}>
        //                 <Icon name='menu' />
        //             </Button>
        //         </Left>
        //         <Body>
        //         <Title style={[human.bodyWhite, systemWeights.bold]}>{this.props.title}</Title>
        //         </Body>
        //         <Right />
        // </Header>
        // )

        return (
            <View style={{display:'flex', flexDirection:'row'}}>
                    
                <Button transparent onPress={this.openMenu}>
                    <Icon name='menu' style={{color : '#000'}}/>
                </Button>
                <View style={{flex:0.8, justifyContent:'center',alignItems:'center'}}>
                    <Text style={[human.body, systemWeights.bold]}>{this.props.title}</Text>
                </View>
            </View>
        )
    }
}