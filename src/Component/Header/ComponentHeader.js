import React from 'react';
import {View} from 'react-native';
import { Header, Title, Left, Right, Body, Icon, Button, Text } from 'native-base';
import {Colors} from "../../config/variables";
import { human, systemWeights  } from 'react-native-typography';

export default class ComponentHeader extends React.Component{
    goBack = ()=>{
        this.props.navigation.goBack();
    };

    render(){
        return (
            <View style={{display:'flex', flexDirection:'row'}}>
                    
                <Button  transparent onPress={this.goBack}>
                    <Icon name='arrow-back' style={{color : '#000'}}/>
                </Button>
                <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                    <Text style={[human.body, systemWeights.bold]}>{this.props.title}</Text>
                </View>
                <Button transparent onPress={this.props.onClickFilter}>
                    <Icon type="MaterialIcons" name='filter-list' style={{color : '#000'}}/>
                </Button>
            </View>
        )
    }
}

ComponentHeader.defaultProps = {
    onClickFilter :()=>{}
}