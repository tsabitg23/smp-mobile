import React from 'react';
import {View} from 'react-native';
import { Header, Title, Left, Right, Body, Icon, Button, Text } from 'native-base';
import {Colors} from "../../config/variables";
import { human, systemWeights  } from 'react-native-typography';

export default class ModalHeader extends React.Component{
    render(){
        return (
            <View style={{display:'flex', flexDirection:'row'}}>
                <View style={{flex:1, justifyContent:'center',alignItems:'center',marginTop: 12}}>
                    <Text style={[human.body, systemWeights.bold]}>{this.props.title}</Text>
                </View>
            </View>
        )
    }
}

ModalHeader.defaultProps = {
    onClickBack :()=>{}
}