import React from 'react';
import {View} from 'react-native';
import { Header, Title, Left, Right, Body, Icon, Button, Text } from 'native-base';
import {Colors} from "../../config/variables";
import { human, systemWeights  } from 'react-native-typography';

export default class ModalHeader extends React.Component{
    render(){
        return (
            <View style={{display:'flex', flexDirection:'row'}}>
                    
                <Button  transparent onPress={this.props.onClickBack}>
                    <Icon name='arrow-back' style={{color : '#000'}}/>
                </Button>
                <View style={{flex:0.9, justifyContent:'center',alignItems:'center'}}>
                    <Text style={[human.body, systemWeights.bold, {textAlign:'center'}]}>{this.props.title}</Text>
                </View>
            </View>
        )
    }
}

ModalHeader.defaultProps = {
    onClickBack :()=>{}
}