import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList, Image} from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Card, CardItem } from 'native-base';
import { human, systemWeights  } from 'react-native-typography';

export default class EmptyCard extends Component{
    render(){
        return (
            <View style={{padding: 30}}>
                <Image style={{width : '100%', height : 300}} source={require('../../assets/empty-state.png')} resizeMode="contain"/>
                <View>
                    <Text style={[human.subhead,systemWeights.semibold,{textAlign : 'center'}]}>{this.props.title}</Text>
                    <Text style={[human.caption1,{textAlign : 'center'}]}>{this.props.subtitle}</Text>

                </View>
            </View>
        )
    }
}